import tkinter as tk
from tkinter import ttk
import tkinter.font as tkFont
from tkinter import messagebox

from Gestores.gestorPrestamos import GestorPrestamo

class Window_ManagementRefund:

    gestor = GestorPrestamo()

    def __init__(self, main):
        self.ventana =  tk.Toplevel(main)
        self.ventana.configure(bg='white')
        #setting title
        self.ventana.title("Devoluciones")
        self.ventana.grab_set()
        #setting window size
        width=642
        height=602
        screenwidth = self.ventana.winfo_screenwidth()
        screenheight = self.ventana.winfo_screenheight()
        alignstr = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        self.ventana.geometry(alignstr)
        self.ventana.resizable(width=False, height=False)

        ftLabel = tkFont.Font(family='Trebuchet MS',size=16)
        ftButton = tkFont.Font(family='Trebuchet MS',size=16)
        ftEntry = tkFont.Font(family='Trebuchet MS',size=16)

        GLabel_860=tk.Label(self.ventana)
        ft = tkFont.Font(family='Trebuchet MS',size=30)
        GLabel_860["font"] = ft
        GLabel_860["fg"] = "#333333"
        GLabel_860["bg"] = "#ffffff"
        GLabel_860["justify"] = "center"
        GLabel_860["text"] = "Devoluciones"
        GLabel_860.place(x=20,y=10,width=592,height=30)


        #--------------------
        frameSearch = tk.Frame(self.ventana)
        frameSearch["bg"] = "white"
        frameSearch.place(x=10,y=50,width=622,height=60)            

        GLabel_712=tk.Label(frameSearch)
        GLabel_712["font"] = ftLabel
        GLabel_712["fg"] = "#333333"
        GLabel_712["bg"] = "#ffffff"
        GLabel_712["justify"] = "left"
        GLabel_712["text"] = "ID prestamo"
        GLabel_712.place(x=10,y=0,width=150,height=25)

        GLabel_909=tk.Label(frameSearch)
        GLabel_909["font"] = ftLabel
        GLabel_909["fg"] = "#333333"
        GLabel_909["justify"] = "left"
        GLabel_909["bg"] = "#ffffff"
        GLabel_909["text"] = "Socio"
        GLabel_909.place(x=180,y=0,width=150,height=25)
       
        btn_buscarPrestamo=tk.Button(frameSearch)
        btn_buscarPrestamo["bg"] = "#284b63"
        btn_buscarPrestamo["font"] = ftButton
        btn_buscarPrestamo["fg"] = "#ffffff"
        btn_buscarPrestamo["justify"] = "center"
        btn_buscarPrestamo["text"] = "Buscar"
        btn_buscarPrestamo.place(x=520,y=30,width=110,height=30)
        btn_buscarPrestamo["command"] = self.btn_buscarDevolucion_click

        self.idPrestamo = tk.StringVar()
        self.idSocio = tk.StringVar()
        
        txt_idPrestamo=tk.Entry(frameSearch)
        txt_idPrestamo["bg"] = "#d9d9d9"
        txt_idPrestamo["font"] = ftEntry
        txt_idPrestamo["fg"] = "#070707"
        txt_idPrestamo["justify"] = "center"
        txt_idPrestamo["text"] = "Entry"
        txt_idPrestamo["textvariable"] = self.codigoBusqueda
        txt_idPrestamo.place(x=10,y=30,width=150,height=30)

        txt_idSocio=tk.Entry(frameSearch)
        txt_idSocio["bg"] = "#d9d9d9"
        txt_idSocio["font"] = ftEntry
        txt_idSocio["fg"] = "#333333"
        txt_idSocio["justify"] = "center"
        txt_idSocio["text"] = "Entry"
        txt_idSocio["textvariable"] = self.nombreBusqueda
        txt_idSocio.place(x=180,y=30,width=150,height=30)

        #Tabla  Devoluciones
        self.frameTable = tk.Frame(self.ventana)
        self.frameTable["bg"] = "white"
        self.frameTable.place_forget()

        self.estilotabla = ttk.Style(self.frameTable)
        self.estilotabla.configure("Treeview",background="#D9D9D9")
        self.estilotabla.map("Treeview",background = [('selected','#3C6E71')])

        tableScroll = tk.Scrollbar(self.frameTable)
        tableScroll.place(x=602,y = 0,width=20,height=230)
        self.tablaDatos = ttk.Treeview(self.frameTable,yscrollcommand=tableScroll.set)
        tableScroll.config(command=self.tablaDatos.yview)
        self.tablaDatos['columns']=('IdPrestamo','Socio','Libro','fecha prestamo','fecha devolucion','dias de retraso', 'fecha extravio', 'estado') #Definir Columnas
        self.tablaDatos.column("IdPrestamo",width=80,anchor="center",stretch="No")
        self.tablaDatos.column("Socio",     width=140,anchor="center",stretch="No")
        self.tablaDatos.column("Libro",   width=140,anchor="center",stretch="No")
        self.tablaDatos.column("fecha prestamo",        width=80,anchor="center",stretch="No")
        self.tablaDatos.column("fecha devolucion",        width=80,anchor="center",stretch="No")
        self.tablaDatos.column("dias de retraso",        width=80,anchor="center",stretch="No")
        self.tablaDatos.column("fecha extravio",        width=80,anchor="center",stretch="No")
        self.tablaDatos.column("estado",        width=80,anchor="center",stretch="No")
        
        #self.tablaDatos.heading("id",         anchor="center",text="")
        self.tablaDatos.heading("IdPrestamo",anchor="center",text="Nroº Prestamo")
        self.tablaDatos.heading("Socio",     anchor="center",text="Socio")
        self.tablaDatos.heading("Libro",   anchor="center",text="Libro")
        self.tablaDatos.heading("fecha prestamo",        anchor="center",text="Fecha Prestamo")
        self.tablaDatos.heading("fecha devolucion",        anchor="center",text="Fecha Devolucion")
        self.tablaDatos.heading("dias de retraso",        anchor="center",text="Dias de retraso")
        self.tablaDatos.heading("fecha extravio",        anchor="center",text="Fecha Extravio")
        self.tablaDatos.heading("estado",        anchor="center",text="Estado")
        
        self.tablaDatos['show']='headings'

        # end for
        self.tablaDatos.place(x=0,y=0,width=602,height=230)
        self.tablaDatos.bind("<Double-1>",self.selectedItemTable)

        #FinTabla Devoluciones
        
        
        #-----------------

        # frameData= tk.Frame(self.ventana)
        # frameData["bg"] = "white"
        # frameData.place(x=10,y=330,width=622,height=260)

    
        # ftLabel = tkFont.Font(family='Trebuchet MS',size=16)
        # ftButton = tkFont.Font(family='Trebuchet MS',size=16)
        # ftEntry = tkFont.Font(family='Trebuchet MS',size=16)


        # GLabel_936=tk.Label(frameData)
        # ft = tkFont.Font(family='Trebuchet MS',size=20)
        # GLabel_936["font"] = ft
        # GLabel_936["fg"] = "#333333"
        # GLabel_936["bg"] = "#ffffff"
        # GLabel_936["justify"] = "center"
        # GLabel_936["text"] = "Agregar Prestamo"
        # GLabel_936.place(x=20,y=-5,width=184,height=35)


        # GLabel_446=tk.Label(frameData)
        # GLabel_446["font"] = ftLabel
        # GLabel_446["fg"] = "#333333"
        # GLabel_446["bg"] = "#ffffff"
        # GLabel_446["justify"] = "left"
        # GLabel_446["text"] = "Documento Socio"
        # GLabel_446.place(x=10,y=30,width=150,height=25)

        # GLabel_102=tk.Label(frameData)
        # GLabel_102["font"] = ftLabel
        # GLabel_102["fg"] = "#333333"
        # GLabel_102["bg"] = "#ffffff"
        # GLabel_102["justify"] = "left"
        # GLabel_102["text"] = "Código Libro"
        # GLabel_102.place(x=200,y=30,width=150,height=25)

        # GLabel_466=tk.Label(frameData)
        # GLabel_466["font"] = ftLabel
        # GLabel_466["fg"] = "#333333"
        # GLabel_466["bg"] = "#ffffff"
        # GLabel_466["justify"] = "left"
        # GLabel_466["text"] = "Dias del prestamo"
        # GLabel_466.place(x=390,y=30,width=150,height=25)

        # self.dniSocio = tk.StringVar()
        # self.idLibro = tk.StringVar()
        # self.cant_dias = tk.StringVar()

        # txt_socio=tk.Entry(frameData)
        # txt_socio["bg"] = "#d9d9d9"
        # txt_socio["borderwidth"] = "1px"
        # txt_socio["font"] = ftEntry
        # txt_socio["fg"] = "#333333"
        # txt_socio["justify"] = "center"
        # txt_socio["text"] = "Entry"
        # txt_socio["textvariable"] = self.dniSocio
        # txt_socio.place(x=10,y=60,width=150,height=30)

        # txt_idLibro=tk.Entry(frameData)
        # txt_idLibro["bg"] = "#d9d9d9"
        # txt_idLibro["borderwidth"] = "1px"
        # txt_idLibro["font"] = ftEntry
        # txt_idLibro["fg"] = "#333333"
        # txt_idLibro["justify"] = "center"
        # txt_idLibro["text"] = "Entry"
        # txt_idLibro["textvariable"] = self.idLibro
        # txt_idLibro.place(x=200,y=60,width=150,height=30)

        # txt_cantDias=tk.Entry(frameData)
        # txt_cantDias["bg"] = "#d9d9d9"
        # txt_cantDias["borderwidth"] = "1px"
        # txt_cantDias["font"] = ftEntry
        # txt_cantDias["fg"] = "#333333"
        # txt_cantDias["justify"] = "center"
        # txt_cantDias["text"] = "Entry"
        # txt_cantDias["textvariable"] = self.cant_dias
        # txt_cantDias.place(x=390,y=60,width=150,height=30)





    def btn_buscarDevolucion_click(self):
        #Agregar Validacion de que no puede venir vacio o obviarlo si viene vacio
        listaPrestamos = []       
        self.tablaDatos.delete(*self.tablaDatos.get_children())
        #metodo de busqueda de datos 
        listaPrestamos = self.gestor.BuscarPrestamo(self.idSocio.get(),self.idPrestamo.get(),self.DNI.get())
        count = 0
        if(len(listaPrestamos)>0):
            for socio in listaPrestamos:
                self.tablaDatos.insert(parent="",index="end",iid=count,text="",
                                values=(socio[0],socio[1],socio[2],socio[3],socio[4],socio[5]))
                count+=1
            # comment: 
            self.frameTable.place(x=10,y=120,width=622,height=230)

    def selectedItemTable(self,event):
        libroSeleccionado = self.tablaDatos.item(self.tablaDatos.focus()).get("values")
        self.numeroLibroSeleccionado  = libroSeleccionado[0]
        self.set_text_entry(libroSeleccionado[0],self.txt_codigoLibro)
        self.set_text_entry(libroSeleccionado[1],self.txt_nombreLibro)
        self.set_text_entry(libroSeleccionado[2],self.txt_autorLibro)
        self.set_text_entry(libroSeleccionado[4],self.txt_precioLibro)
        self.cb_estadoLibro.set(libroSeleccionado[3])
        self.estadoLibroSeleccionado = libroSeleccionado[3]
        self.btn_agregarLibro["text"] = "Modificar Libro"

    def btn_agregarPrestamo_click(self):
        print("Socio " + self.dniSocio.get())
        print("Codigo libro " +self.idLibro.get())
        print("Cantidad de días " +self.cant_dias.get())

        if(self.gestor.agregarPrestamo(self.idLibro.get(), self.dniSocio.get(), self.cant_dias.get())):
            messagebox.showinfo(title="Resultado",message="Se Agrego el prestamo Con exito",parent=self.ventana)
            self.cleanVariables()
        else:
            messagebox.showerror(title="Error",message="Sucedio un Error Al Agregar el prestamo",parent=self.ventana)

    def set_text_entry(self,text,entry):
        entry.delete(0,tk.END)
        entry.insert(0,text)

    def cleanVariables(self):
        self.set_text_entry("",self.dniSocio)
        self.set_text_entry("",self.idLibro)
        self.set_text_entry("",self.cant_dias)

