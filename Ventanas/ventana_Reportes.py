import tkinter as tk
from tkinter import ttk
import tkinter.font as tkFont
from Gestores.gestorReportes import GestorReportes
from tkinter import messagebox

class Window_Reports:

    ListadoDeCantidadLibros = []
    ListadoDePedidoDeUnLibro = []
    ListadoPrestamosDemorados = []
    ListadoPrestamosDeUnSocio = []
    gestor = GestorReportes()  

    def __init__(self, main):
        self.ventana =  tk.Toplevel(main)
        self.ventana.configure(bg='white')
        #setting title
        self.ventana.title("Reportes")
        self.ventana.grab_set()
        #setting window size
        width=640
        height=600
        screenwidth = self.ventana.winfo_screenwidth()
        screenheight = self.ventana.winfo_screenheight()
        alignstr = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        self.ventana.geometry(alignstr)
        self.ventana.resizable(width=False, height=False)

        ftLabel = tkFont.Font(family='Trebuchet MS',size=16)
        ftLabelSub = tkFont.Font(family='Trebuchet MS',size=22)
        ftButton = tkFont.Font(family='Trebuchet MS',size=16)
        ftEntry = tkFont.Font(family='Trebuchet MS',size=16)

        ### Titulo 
        lbl_title=tk.Label(self.ventana)
        ft = tkFont.Font(family='Trebuchet MS',size=30)
        lbl_title["font"] = ft
        lbl_title["fg"] = "#333333"
        lbl_title["bg"] = "#D9D9D9"
        lbl_title["justify"] = "center"
        lbl_title["text"] = "Reportes"
        lbl_title.place(x=20,y=0,width=592,height=50)
        
        
        
        frameStateBooks = tk.Frame(self.ventana)
        frameStateBooks["bg"] = "white"
        frameStateBooks.place(x=10,y=50,width=622,height=80)
        
        GLabel_712=tk.Label(frameStateBooks)
        GLabel_712["font"] = ftLabelSub
        GLabel_712["fg"] = "#333333"
        GLabel_712["bg"] = "#ffffff"
        GLabel_712["justify"] = "left"
        GLabel_712["text"] = "Estado de Libros"
        GLabel_712.place(x=20,y=0,width=582,height=30)
        
        GLabel_591=tk.Label(frameStateBooks)
        GLabel_591["font"] = ftLabel
        GLabel_591["fg"] = "#333333"
        GLabel_591["bg"] = "#ffffff"
        GLabel_591["justify"] = "center"
        GLabel_591["text"] = "Disponibles:"
        GLabel_591.place(x=0,y=40,width=115,height=25)

        self.ListadoDeCantidadLibros = self.gestor.dalLibros.ObtenerCantidadLibros()

        lbl_cantidadDisponibles=tk.Label(frameStateBooks)
        lbl_cantidadDisponibles["font"] = ftLabel
        lbl_cantidadDisponibles["fg"] = "#333333"
        lbl_cantidadDisponibles["bg"] = "#ffffff"
        lbl_cantidadDisponibles["justify"] = "center"
        lbl_cantidadDisponibles["text"] = self.ListadoDeCantidadLibros[0]
        lbl_cantidadDisponibles.place(x=125,y=40,width=40,height=25)

        GLabel_595=tk.Label(frameStateBooks)
        GLabel_595["font"] = ftLabel
        GLabel_595["fg"] = "#333333"
        GLabel_595["bg"] = "#ffffff"
        GLabel_595["justify"] = "center"
        GLabel_595["text"] = "Prestados:"
        GLabel_595.place(x=180,y=40,width=115,height=25)

        lbl_cantidadPrestados=tk.Label(frameStateBooks)
        lbl_cantidadPrestados["font"] = ftLabel
        lbl_cantidadPrestados["fg"] = "#333333"
        lbl_cantidadPrestados["bg"] = "#ffffff"
        lbl_cantidadPrestados["justify"] = "center"
        lbl_cantidadPrestados["text"] = self.ListadoDeCantidadLibros[1]
        lbl_cantidadPrestados.place(x=305,y=40,width=40,height=25)

        GLabel_964=tk.Label(frameStateBooks)
        GLabel_964["font"] = ftLabel
        GLabel_964["fg"] = "#333333"
        GLabel_964["bg"] = "#ffffff"
        GLabel_964["justify"] = "center"
        GLabel_964["text"] = "Extraviados:"
        GLabel_964.place(x=360,y=40,width=115,height=25)

        lbl_cantidadExtraviados=tk.Label(frameStateBooks)
        lbl_cantidadExtraviados["font"] = ftLabel
        lbl_cantidadExtraviados["fg"] = "#333333"
        lbl_cantidadExtraviados["bg"] = "#ffffff"
        lbl_cantidadExtraviados["justify"] = "center"
        lbl_cantidadExtraviados["text"] = self.ListadoDeCantidadLibros[2]
        lbl_cantidadExtraviados.place(x=485,y=40,width=40,height=25)

        btn_reportre_lib = tk.Button(frameStateBooks)
        btn_reportre_lib["bg"] = "#284b63"
        btn_reportre_lib["font"] = ftButton
        btn_reportre_lib["fg"] = "#ffffff"
        btn_reportre_lib["justify"] = "center"
        btn_reportre_lib["text"] = "Reporte"
        btn_reportre_lib.place(x=535,y=40,width=85,height=27)
        btn_reportre_lib["command"] = self.btn_cantidadLibros_click
        
        
        frameBooksMissing = tk.Frame(self.ventana)
        frameBooksMissing["bg"] = "#D9D9D9"
        frameBooksMissing.place(x=10,y=130,width=622,height=80)
        
        
        GLabel_794=tk.Label(frameBooksMissing)
        GLabel_794["font"] = ftLabelSub
        GLabel_794["fg"] = "#333333"
        GLabel_794["bg"] = "#D9D9D9"
        GLabel_794["justify"] = "left"
        GLabel_794["text"] = "Extravio de Libros"
        GLabel_794.place(x=20,y=0,width=582,height=30)

        GLabel_869=tk.Label(frameBooksMissing)
        GLabel_869["font"] = ftLabel
        GLabel_869["fg"] = "#333333"
        GLabel_869["bg"] = "#D9D9D9"
        GLabel_869["justify"] = "center"
        GLabel_869["text"] = "Reposicion de Los Libros Extraviados :"
        GLabel_869.place(x=20,y=40,width=360,height=30)

        GLabel_725=tk.Label(frameBooksMissing)
        GLabel_725["font"] = ftLabel
        GLabel_725["fg"] = "#333333"
        GLabel_725["bg"] = "#D9D9D9"
        GLabel_725["justify"] = "center"
        GLabel_725["text"] = f"$ {self.gestor.GenerarSumatoriaPrecioReposicion()}"
        GLabel_725.place(x=390,y=40,width=99,height=30)
        
        frameLists = tk.Frame(self.ventana)
        frameLists["bg"] = "white"
        frameLists.place(x=10,y=210,width=622,height=380)
             
        GLabel_204=tk.Label(frameLists)
        GLabel_204["font"] = ftLabelSub
        GLabel_204["fg"] = "#333333"
        GLabel_204["bg"] = "#ffffff"
        GLabel_204["justify"] = "center"
        GLabel_204["text"] = "Listados"
        GLabel_204.place(x=20,y=0,width=582,height=25)
        
        self.opcionList  = tk.IntVar()
        
        GRadio_661=tk.Radiobutton(frameLists)
        GRadio_661["font"] = ftLabel
        GRadio_661["fg"] = "#333333"
        GRadio_661["bg"] = "#ffffff"
        GRadio_661["justify"] = "center"
        GRadio_661["text"] = "Libro Solicitados"
        GRadio_661["value"]= 0
        GRadio_661["variable"]=self.opcionList
        GRadio_661.place(x=00,y=35,width=200,height=30)
        GRadio_661["command"] = self.select_rb_list

        GRadio_172=tk.Radiobutton(frameLists)
        GRadio_172["font"] = ftLabel
        GRadio_172["fg"] = "#333333"
        GRadio_172["bg"] = "#ffffff"
        GRadio_172["justify"] = "center"
        GRadio_172["text"] = "Prestamos a Socio"
        GRadio_172["value"]= 1
        GRadio_172["variable"]=self.opcionList
        GRadio_172.place(x=190,y=35,width=200,height=30)
        GRadio_172["command"] = self.select_rb_list

        GRadio_369=tk.Radiobutton(frameLists)
        GRadio_369["font"] = ftLabel
        GRadio_369["fg"] = "#333333"
        GRadio_369["bg"] = "#ffffff"
        GRadio_369["justify"] = "center"
        GRadio_369["text"] = "Prestamos Demorados"
        GRadio_369["value"]=2
        GRadio_369["variable"]=self.opcionList
        GRadio_369.place(x=385,y=35,width=240,height=30)
        GRadio_369["command"] = self.select_rb_list



        self.frameBooks = tk.Frame(frameLists)
        self.frameBooks["bg"] = "#D9D9D9"
        self.frameBooks.place(x=0,y=70,width=622,height=380)

        GLabel_887=tk.Label(self.frameBooks)
        GLabel_887["font"] = ftLabel
        GLabel_887["fg"] = "#353535"
        GLabel_887["bg"] = "#D9D9D9"
        GLabel_887["justify"] = "left"
        GLabel_887["text"] = "Titulo de Libro"
        GLabel_887.place(x=20,y=20,width=250,height=30)

        self.nombreLibro = tk.StringVar()

        txt_nombreLibro=tk.Entry(self.frameBooks)
        txt_nombreLibro["bg"] = "#353535"
        txt_nombreLibro["borderwidth"] = "1px"
        txt_nombreLibro["font"] = ftEntry
        txt_nombreLibro["fg"] = "#FFFFFF"
        txt_nombreLibro["justify"] = "center"
        txt_nombreLibro["text"] = "Entry"
        txt_nombreLibro["textvariable"] = self.nombreLibro
        txt_nombreLibro.place(x=240,y=20,width=180,height=30)

        btn_BuscarLibro=tk.Button(self.frameBooks)
        btn_BuscarLibro["bg"] = "#284b63"
        btn_BuscarLibro["font"] = ftButton
        btn_BuscarLibro["fg"] = "#ffffff"
        btn_BuscarLibro["justify"] = "center"
        btn_BuscarLibro["text"] = "Generar Reporte"
        btn_BuscarLibro.place(x=460,y=20,width=150,height=30)
        btn_BuscarLibro["command"] = self.btn_BuscarLibro_click
        
        if(len(self.ListadoDePedidoDeUnLibro)> 0):
            count = 0
            tableScroll = tk.Scrollbar(self.frameBooks)
            tableScroll.place(x=602,y = 0,width=20,height=230)
            tablaDatos = ttk.Treeview(self.frameBooks,yscrollcommand=tableScroll.set)
            tableScroll.config(command=tablaDatos.yview)
            tablaDatos['columns']=('id','NumeroSocio','Nombre','Apellido','DNI',) #Definir Columnas
            tablaDatos.column("id",         width=00,anchor="center")
            tablaDatos.column("NumeroSocio",width=80,anchor="center")
            tablaDatos.column("Nombre",     width=80,anchor="center")
            tablaDatos.column("Apellido",   width=80,anchor="center")
            tablaDatos.column("DNI",        width=80,anchor="center")
            
            tablaDatos.heading("id",         anchor="center",text="")
            tablaDatos.heading("NumeroSocio",anchor="center",text="Nroº Socio")
            tablaDatos.heading("Nombre",     anchor="center",text="Nombre")
            tablaDatos.heading("Apellido",   anchor="center",text="Apellido")
            tablaDatos.heading("DNI",        anchor="center",text="DNI")

            for socio in self.listaSocios:
                tablaDatos.insert(parent="",index="end",iid=count,text="",
                                  values=(socio.id,socio.numeroSocio,socio.Nombre,socio.Apellido,socio.DNI))
                count+=1
                # comment: 
            # end for
            tablaDatos.place(x=0,y=0,width=582,height=230)


        self.frameMember = tk.Frame(frameLists)
        self.frameMember["bg"] = "#D9D9D9"
        self.frameMember.place_forget()

        GLabel_534=tk.Label(self.frameMember)
        GLabel_534["font"] = ftLabel
        GLabel_534["fg"] = "#333333"
        GLabel_534["bg"] = "#D9D9D9"
        GLabel_534["justify"] = "left"
        GLabel_534["text"] = "Numero de Socio"
        GLabel_534.place(x=20,y=20,width=250,height=30)

        self.numeroSocio = tk.StringVar()

        txt_numeroSocio=tk.Entry(self.frameMember)
        txt_numeroSocio["bg"] = "#353535"
        txt_numeroSocio["borderwidth"] = "1px"
        txt_numeroSocio["font"] = ftEntry
        txt_numeroSocio["fg"] = "#FFFFFF"
        txt_numeroSocio["justify"] = "center"
        txt_numeroSocio["text"] = "Entry"
        txt_numeroSocio["textvariable"] = self.numeroSocio
        txt_numeroSocio.place(x=240,y=20,width=180,height=30)

        btn_BuscarSocio=tk.Button(self.frameMember)
        btn_BuscarSocio["bg"] = "#284b63"
        btn_BuscarSocio["font"] = ftButton
        btn_BuscarSocio["fg"] = "#ffffff"
        btn_BuscarSocio["justify"] = "center"
        btn_BuscarSocio["text"] = "Buscar"
        btn_BuscarSocio.place(x=460,y=20,width=150,height=30)
        btn_BuscarSocio["command"] = self.btn_BuscarPrestamoDeSocio_click

        if(len(self.ListadoPrestamosDemorados)> 0):
            count = 0
            tableScroll = tk.Scrollbar(self.frameMember)
            tableScroll.place(x=602,y = 55,width=20,height=330)
            tablaDatos = ttk.Treeview(self.frameMember,yscrollcommand=tableScroll.set)
            tableScroll.config(command=tablaDatos.yview)
            tablaDatos['columns']=('id','NumeroSocio','Libro','FechaVencimiento','Estado',) #Definir Columnas
            tablaDatos.column("id",         width=00,anchor="center")
            tablaDatos.column("NumeroSocio",width=80,anchor="center")
            tablaDatos.column("Libro",     width=80,anchor="center")
            tablaDatos.column("FechaVencimiento",   width=80,anchor="center")
            tablaDatos.column("Estado",        width=80,anchor="center")

            tablaDatos.heading("id",         anchor="center",text="")
            tablaDatos.heading("NumeroSocio",anchor="center",text="Nroº Socio")
            tablaDatos.heading("Libro",     anchor="center",text="Libro")
            tablaDatos.heading("FechaVencimiento",   anchor="center",text="Fecha Vencimiento")
            tablaDatos.heading("Estado",        anchor="center",text="Estado")

            for prestamo in self.ListadoPrestamosDemorados:
                tablaDatos.insert(parent="",index="end",iid=count,text="",
                                  values=(prestamo.id,prestamo.numeroSocio,prestamo.Libro,prestamo.FechaVencimiento,prestamo.Estado))
                count+=1
                # comment: 
            # end for
            tablaDatos.place(x=0,y=55,width=582,height=330)

        self.frameDelayed = tk.Frame(frameLists)
        self.frameDelayed["bg"] = "#D9D9D9"
        self.frameDelayed.place_forget()

        GLabel_151=tk.Label(self.frameDelayed)
        GLabel_151["font"] = ftLabel
        GLabel_151["fg"] = "#333333"
        GLabel_151["bg"] = "#D9D9D9"
        GLabel_151["justify"] = "left"
        GLabel_151["text"] = "Prestamos Demorados"
        GLabel_151.place(x=20,y=20,width=250,height=30)
        
        btn_BuscarLibro=tk.Button(self.frameDelayed)
        btn_BuscarLibro["bg"] = "#284b63"
        btn_BuscarLibro["font"] = ftButton
        btn_BuscarLibro["fg"] = "#ffffff"
        btn_BuscarLibro["justify"] = "center"
        btn_BuscarLibro["text"] = "Generar Reporte"
        btn_BuscarLibro.place(x=460,y=20,width=150,height=30)
        btn_BuscarLibro["command"] = self.btn_BuscarPrestamosDemorados_click
        # if(len(self.ListadoPrestamosDemorados)> 0):
        #     count = 0
        #     tableScroll = tk.Scrollbar(self.frameDelayed)
        #     tableScroll.place(x=602,y = 55,width=20,height=330)
        #     tablaDatos = ttk.Treeview(self.frameDelayed,yscrollcommand=tableScroll.set)
        #     tableScroll.config(command=tablaDatos.yview)
        #     tablaDatos['columns']=('id','NumeroSocio','Libro','FechaVencimiento','Estado',) #Definir Columnas
        #     tablaDatos.column("id",         width=00,anchor="center")
        #     tablaDatos.column("NumeroSocio",width=80,anchor="center")
        #     tablaDatos.column("Libro",     width=80,anchor="center")
        #     tablaDatos.column("FechaVencimiento",   width=80,anchor="center")
        #     tablaDatos.column("Estado",        width=80,anchor="center")
            
        #     tablaDatos.heading("id",         anchor="center",text="")
        #     tablaDatos.heading("NumeroSocio",anchor="center",text="Nroº Socio")
        #     tablaDatos.heading("Libro",     anchor="center",text="Libro")
        #     tablaDatos.heading("FechaVencimiento",   anchor="center",text="Fecha Vencimiento")
        #     tablaDatos.heading("Estado",        anchor="center",text="Estado")

        #     for prestamo in self.ListadoPrestamosDemorados:
        #         tablaDatos.insert(parent="",index="end",iid=count,text="",
        #                           values=(prestamo.id,prestamo.numeroSocio,prestamo.Libro,prestamo.FechaVencimiento,prestamo.Estado))
        #         count+=1
        #         # comment: 
        #     # end for
        #     tablaDatos.place(x=0,y=55,width=582,height=330)

        # self.gestor.ObtenerListadoPrestamosDemorados()

    def btn_BuscarPrestamosDemorados_click(self):
        pathReporte = f"prestamos-demorados.pdf"
        result = self.gestor.ObtenerListadoPrestamosDemorados(pathReporte)

        if result:
            messagebox.showinfo(title="Resultado",message=f"Se Genero el reporte en Recursos/Reportes/{pathReporte}",parent=self.ventana)
        else:
            messagebox.showerror(title="Error",message="Sucedio un Error al generar el reporte",parent=self.ventana)


    def btn_BuscarLibro_click(self):
        pathReporte = f"socios-libro-{self.nombreLibro.get()}.pdf"
        result = self.gestor.ObtenerLibroDeSocios(pathReporte, self.nombreLibro.get())

        if result:
            messagebox.showinfo(title="Resultado",message=f"Se Genero el reporte en Recursos/Reportes/{pathReporte}",parent=self.ventana)
        else:
            messagebox.showerror(title="Error",message="Sucedio un Error al generar el reporte",parent=self.ventana)

    def btn_BuscarPrestamoDeSocio_click(self):
        if(len(self.ListadoPrestamosDeUnSocio)> 0):
            count = 0
            tableScroll = tk.Scrollbar(self.frameMember)
            tableScroll.place(x=602,y = 55,width=20,height=330)
            tablaDatos = ttk.Treeview(self.frameMember,yscrollcommand=tableScroll.set)
            tableScroll.config(command=tablaDatos.yview)
            tablaDatos['columns']=('idPrestamo','NumeroSocio','Libro','FechaPrestamo','Estado',) #Definir Columnas
            tablaDatos.column("id",         width=00,anchor="center")
            tablaDatos.column("NumeroSocio",width=80,anchor="center")
            tablaDatos.column("Libro",     width=80,anchor="center")
            tablaDatos.column("FechaPrestamo",   width=80,anchor="center")
            tablaDatos.column("Estado",        width=80,anchor="center")

            tablaDatos.heading("id",         anchor="center",text="")
            tablaDatos.heading("NumeroSocio",anchor="center",text="Nroº Socio")
            tablaDatos.heading("Libro",     anchor="center",text="Libro")
            tablaDatos.heading("FechaPrestamo",   anchor="center",text="Fecha Prestamo]")
            tablaDatos.heading("Estado",        anchor="center",text="Estado")

            for prestamo in self.ListadoPrestamosDeUnSocio:
                tablaDatos.insert(parent="",index="end",iid=count,text="",
                                  values=(prestamo.id,prestamo.idSocio,prestamo.idLibro,prestamo.FechaPrestamo,prestamo.estado))
                count+=1
            # end for
            tablaDatos.place(x=0,y=55,width=582,height=330)

        pathReporte = f"prestamo-socio-{self.numeroSocio.get()}.pdf"
        result = self.gestor.GenerarListadoPrestamoDeSocio(pathReporte,  self.numeroSocio.get())

        if result:
            messagebox.showinfo(title="Resultado",message=f"Se Genero el reporte en Recursos/Reportes/{pathReporte}",parent=self.ventana)
        else:
            messagebox.showerror(title="Error",message="Sucedio un Error al generar el reporte",parent=self.ventana)

    def select_rb_list(self):
        match self.opcionList.get():
            case 0:
                self.frameBooks.place(x=0,y=70,width=622,height=380)
                self.frameMember.place_forget()
                self.frameDelayed.place_forget()         
            case 1:
                self.frameBooks.place_forget()
                self.frameMember.place(x=0,y=70,width=622,height=380)
                self.frameDelayed.place_forget()   
            case 2:
                self.frameBooks.place_forget()
                self.frameMember.place_forget()
                self.frameDelayed.place(x=0,y=70,width=622,height=380)

    def btn_cantidadLibros_click(self):
        self.gestor.GenerarReporte()