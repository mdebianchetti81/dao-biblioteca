import tkinter as tk
import re
from tkinter import ttk
import tkinter.font as tkFont
from tkinter import messagebox
from datetime import datetime, timedelta

from Gestores.gestorPrestamos import GestorPrestamo

class Window_ManagementLoans:

    listaPrestamos = []
    codigoSeleccionado = ""
    estadoPrestamoSeleccionado = ""
    stateEntryDNISocioSearch = True
    stateEntryIDlibroSearch = True

    stateEntryDNISocio = True
    stateEntryIDlibro = True
    stateEntryFechaDevolucion = True
    stateEntryFechaExtravio = True
    stateEntryFechaSolicitud = True
    stateEntryFechaDiasRetraso = True

    stateEntryidLibroBusqueda = True

    gestor = GestorPrestamo()

    def __init__(self, main):
        self.ventana =  tk.Toplevel(main)
        self.ventana.configure(bg='white')
        #setting title
        self.ventana.title("Adminsitracion de Prestamos")
        self.ventana.grab_set()
        #setting window size
        width=642
        height=602
        screenwidth = self.ventana.winfo_screenwidth()
        screenheight = self.ventana.winfo_screenheight()
        alignstr = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        self.ventana.geometry(alignstr)
        self.ventana.resizable(width=False, height=False)
        ftLabel = tkFont.Font(family='Trebuchet MS',size=16)
        ftButton = tkFont.Font(family='Trebuchet MS',size=16)
        ftEntry = tkFont.Font(family='Trebuchet MS',size=16)

        GLabel_860=tk.Label(self.ventana)
        ft = tkFont.Font(family='Trebuchet MS',size=30)
        GLabel_860["font"] = ft
        GLabel_860["fg"] = "#333333"
        GLabel_860["bg"] = "#ffffff"
        GLabel_860["justify"] = "center"
        GLabel_860["text"] = "Administración de Prestamos"
        GLabel_860.place(x=20,y=10,width=592,height=30)


        #--------------------
        frameSearch = tk.Frame(self.ventana)
        frameSearch["bg"] = "white"
        frameSearch.place(x=10,y=50,width=622,height=85)
        GLabel_712=tk.Label(frameSearch)
        GLabel_712["font"] = ftLabel
        GLabel_712["fg"] = "#333333"
        GLabel_712["bg"] = "#ffffff"
        GLabel_712["justify"] = "left"
        GLabel_712["text"] = "DNI Socio"
        GLabel_712.place(x=10,y=0,width=150,height=25)

        GLabel_909=tk.Label(frameSearch)
        GLabel_909["font"] = ftLabel
        GLabel_909["fg"] = "#333333"
        GLabel_909["justify"] = "left"
        GLabel_909["bg"] = "#ffffff"
        GLabel_909["text"] = "ID Libro"
        GLabel_909.place(x=180,y=0,width=150,height=25)

        GLabel_909=tk.Label(frameSearch)
        GLabel_909["font"] = ftLabel
        GLabel_909["fg"] = "#333333"
        GLabel_909["justify"] = "left"
        GLabel_909["bg"] = "#ffffff"
        GLabel_909["text"] = "Estado"
        GLabel_909.place(x=350,y=0,width=150,height=25)

        self.btn_buscarPrestamo=tk.Button(frameSearch)
        self.btn_buscarPrestamo["bg"] = "#284b63"
        ft = tkFont.Font(family='Trebuchet MS',size=16)
        self.btn_buscarPrestamo["font"] = ftButton
        self.btn_buscarPrestamo["fg"] = "#ffffff"
        self.btn_buscarPrestamo["justify"] = "center"
        self.btn_buscarPrestamo["text"] = "Buscar"
        self.btn_buscarPrestamo.place(x=520,y=30,width=110,height=30)
        self.btn_buscarPrestamo["command"] = self.btn_buscarPrestamo_click

        self.codigoBusqueda = tk.StringVar()
        self.dniSocioBusqueda = tk.StringVar()
        self.idLibroBusqueda = tk.StringVar()
        self.estadoBusqueda = tk.StringVar()

        # self.txt_codigoBusqueda=tk.Entry(frameSearch)
        # self.txt_codigoBusqueda["bg"] = "#d9d9d9"
        # self.txt_codigoBusqueda["font"] = ftEntry
        # self.txt_codigoBusqueda["fg"] = "#070707"
        # self.txt_codigoBusqueda["justify"] = "center"
        # self.txt_codigoBusqueda["text"] = "Entry"
        # self.txt_codigoBusqueda["textvariable"] = self.codigoBusqueda
        # # self.txt_codigoBusqueda.place(x=10,y=30,width=150,height=30)

        self.txt_dniSocioBusqueda=tk.Entry(frameSearch)
        self.txt_dniSocioBusqueda["bg"] = "#d9d9d9"
        self.txt_dniSocioBusqueda["font"] = ftEntry
        self.txt_dniSocioBusqueda["fg"] = "#333333"
        self.txt_dniSocioBusqueda["justify"] = "center"
        #self.txt_dniSocioBusqueda["text"] = "Entry"
        self.txt_dniSocioBusqueda["validatecommand"] =self.validate_DNISocioSearch
        self.txt_dniSocioBusqueda["validate"] ="focusout"
        #self.txt_dniSocioBusqueda["textvariable"] = self.dniSocioBusqueda
        self.txt_dniSocioBusqueda.place(x=10,y=30,width=150,height=30)

        self.labelWarnigDNI = tk.Label(frameSearch)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigDNI["font"] = ft
        self.labelWarnigDNI["fg"] = "#FF0000"
        self.labelWarnigDNI["bg"] = "#ffffff"
        self.labelWarnigDNI["justify"] = "center"
        self.labelWarnigDNI["text"] = ""
        self.labelWarnigDNI.place(x=10,y=65,width=150,height=15)

        self.txt_idLibroBusqueda=tk.Entry(frameSearch)
        self.txt_idLibroBusqueda["bg"] = "#d9d9d9"
        self.txt_idLibroBusqueda["font"] = ftEntry
        self.txt_idLibroBusqueda["fg"] = "#333333"
        self.txt_idLibroBusqueda["justify"] = "center"
        self.txt_idLibroBusqueda["validatecommand"] =self.validate_IdLibroSearch
        self.txt_idLibroBusqueda["validate"] ="focus"
        self.txt_idLibroBusqueda.place(x=180,y=30,width=150,height=30)
        
        self.labelWarnigIDLibro = tk.Label(frameSearch)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigIDLibro["font"] = ft
        self.labelWarnigIDLibro["fg"] = "#FF0000"
        self.labelWarnigIDLibro["bg"] = "#ffffff"
        self.labelWarnigIDLibro["justify"] = "center"
        self.labelWarnigIDLibro["text"] = ""
        self.labelWarnigIDLibro.place(x=180,y=65,width=150,height=15)

        options=["En Curso","Demorado","Finalizado"]
        self.cb_estadoBusqueda=ttk.Combobox(frameSearch,values=options,state="readonly",textvariable=self.estadoBusqueda)
        self.cb_estadoBusqueda["font"] = ftEntry
        self.cb_estadoBusqueda.set("Eliga Un Estado")
        self.cb_estadoBusqueda.place(x=350,y=30,width=150,height=30)

        self.gestor.actualizarPrestamosEnCursoADemorados()
        self.gestor.actualizarPrestamosDemoradosAExtraviados()
        #-----------------
        #Tabla Prestamos
        self.frameTable = tk.Frame(self.ventana)
        self.frameTable["bg"] = "white"
        self.frameTable.place_forget()

        self.estilotabla = ttk.Style(self.frameTable)
        self.estilotabla.configure("Treeview",background="#D9D9D9")
        self.estilotabla.map("Treeview",background = [('selected','#3C6E71')])

        tableScroll = tk.Scrollbar(self.frameTable)
        tableScroll.place(x=602,y = 0,width=20,height=210)
        self.tablaDatos = ttk.Treeview(self.frameTable,yscrollcommand=tableScroll.set)
        tableScroll.config(command=self.tablaDatos.yview)
        self.tablaDatos['columns']=('IdPrestamo','Socio','Libro','fechaPrestamo','fechaDevolucion','diasRetraso', 'fechaExtravio', 'estado')
        self.tablaDatos.column("IdPrestamo",width=40,anchor="center",stretch="No")
        self.tablaDatos.column("Socio",     width=40,anchor="center",stretch="No")
        self.tablaDatos.column("Libro",   width=40,anchor="center",stretch="No")
        self.tablaDatos.column("fechaPrestamo",        width=100,anchor="center",stretch="No")
        self.tablaDatos.column("fechaDevolucion",        width=110,anchor="center",stretch="No")
        self.tablaDatos.column("diasRetraso",        width=90,anchor="center",stretch="No")
        self.tablaDatos.column("fechaExtravio",        width=90,anchor="center",stretch="No")
        self.tablaDatos.column("estado",        width=80,anchor="center",stretch="No")

        self.tablaDatos.heading("IdPrestamo",anchor="center",text="Nroº Prestamo")
        self.tablaDatos.heading("Socio",     anchor="center",text="Socio")
        self.tablaDatos.heading("Libro",   anchor="center",text="Libro")
        self.tablaDatos.heading("fechaPrestamo",        anchor="center",text="Fecha Prestamo")
        self.tablaDatos.heading("fechaDevolucion",        anchor="center",text="Fecha Devolucion")
        self.tablaDatos.heading("diasRetraso",        anchor="center",text="Dias de retraso")
        self.tablaDatos.heading("fechaExtravio",        anchor="center",text="Fecha Extravio")
        self.tablaDatos.heading("estado",        anchor="center",text="Estado")

        self.tablaDatos['show']='headings'

        # end for
        self.tablaDatos.place(x=0,y=0,width=602,height=210)
        self.tablaDatos.bind("<Double-1>",self.selectedItemTable)

        #fin tabla prestamos

        #-----------------
        #Editor Prestamos
        frameData= tk.Frame(self.ventana)
        frameData["bg"] = "white"
        frameData.place(x=10,y=350,width=622,height=260)

        ftLabel = tkFont.Font(family='Trebuchet MS',size=16)
        ftButton = tkFont.Font(family='Trebuchet MS',size=16)
        ftEntry = tkFont.Font(family='Trebuchet MS',size=16)


        GLabel_936=tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=20)
        GLabel_936["font"] = ft
        GLabel_936["fg"] = "#333333"
        GLabel_936["bg"] = "#ffffff"
        GLabel_936["justify"] = "center"
        GLabel_936["text"] = "Agregar Prestamo"
        GLabel_936.place(x=20,y=-5,width=250,height=35)


        GLabel_446=tk.Label(frameData)
        GLabel_446["font"] = ftLabel
        GLabel_446["fg"] = "#333333"
        GLabel_446["bg"] = "#ffffff"
        GLabel_446["justify"] = "left"
        GLabel_446["text"] = "Documento Socio"
        GLabel_446.place(x=0,y=30,width=170,height=25)

        GLabel_102=tk.Label(frameData)
        GLabel_102["font"] = ftLabel
        GLabel_102["fg"] = "#333333"
        GLabel_102["bg"] = "#ffffff"
        GLabel_102["justify"] = "left"
        GLabel_102["text"] = "Código Libro"
        GLabel_102.place(x=200,y=30,width=150,height=25)

        GLabel_466=tk.Label(frameData)
        GLabel_466["font"] = ftLabel
        GLabel_466["fg"] = "#333333"
        GLabel_466["bg"] = "#ffffff"
        GLabel_466["justify"] = "left"
        GLabel_466["text"] = "Estado"
        GLabel_466.place(x=10,y=110,width=150,height=25)

        GLabel_466=tk.Label(frameData)
        GLabel_466["font"] = ftLabel
        GLabel_466["fg"] = "#333333"
        GLabel_466["bg"] = "#ffffff"
        GLabel_466["justify"] = "left"
        GLabel_466["text"] = "Fecha Solicitud"
        GLabel_466.place(x=390,y=110,width=170,height=25)

        GLabel_466=tk.Label(frameData)
        GLabel_466["font"] = ftLabel
        GLabel_466["fg"] = "#333333"
        GLabel_466["bg"] = "#ffffff"
        GLabel_466["justify"] = "left"
        GLabel_466["text"] = "Fecha Devolución"
        GLabel_466.place(x=390,y=30,width=170,height=25)

        GLabel_466=tk.Label(frameData)
        GLabel_466["font"] = ftLabel
        GLabel_466["fg"] = "#333333"
        GLabel_466["bg"] = "#ffffff"
        GLabel_466["justify"] = "left"
        GLabel_466["text"] = "Fecha Extravio"
        GLabel_466.place(x=200,y=110,width=150,height=25)

        GLabel_466=tk.Label(frameData)
        GLabel_466["font"] = ftLabel
        GLabel_466["fg"] = "#333333"
        GLabel_466["bg"] = "#ffffff"
        GLabel_466["justify"] = "left"
        GLabel_466["text"] = "Dias Retraso"
        GLabel_466.place(x=10,y=190,width=150,height=25)

        self.dniSocio = tk.StringVar()
        self.idLibro = tk.StringVar()
        self.cant_dias = tk.StringVar()
        self.diasRestraso = tk.StringVar()
        self.fechaDevolucion = tk.StringVar()
        self.fechaPrestamo = tk.StringVar()
        self.fechaExtravio = tk.StringVar()

        self.txt_socio=tk.Entry(frameData)
        self.txt_socio["bg"] = "#d9d9d9"
        self.txt_socio["borderwidth"] = "1px"
        self.txt_socio["font"] = ftEntry
        self.txt_socio["fg"] = "#333333"
        self.txt_socio["justify"] = "center"
        self.txt_socio["text"] = "Entry"
        self.txt_socio["validatecommand"] =self.validate_DNISocioAdd
        self.txt_socio["validate"] ="focus"
        self.txt_socio["textvariable"] = self.dniSocio
        self.txt_socio.place(x=10,y=60,width=150,height=30)

        self.txt_idLibro=tk.Entry(frameData)
        self.txt_idLibro["bg"] = "#d9d9d9"
        self.txt_idLibro["borderwidth"] = "1px"
        self.txt_idLibro["font"] = ftEntry
        self.txt_idLibro["fg"] = "#333333"
        self.txt_idLibro["justify"] = "center"
        self.txt_idLibro["text"] = "Entry"
        self.txt_idLibro["validatecommand"] =self.validate_idLibroAdd
        self.txt_idLibro["validate"] ="focus"
        self.txt_idLibro["textvariable"] = self.idLibro
        self.txt_idLibro.place(x=200,y=60,width=150,height=30)

        self.txt_fechaPrestamo=tk.Entry(frameData)
        self.txt_fechaPrestamo["bg"] = "#d9d9d9"
        self.txt_fechaPrestamo["borderwidth"] = "1px"
        self.txt_fechaPrestamo["font"] = ftEntry
        self.txt_fechaPrestamo["fg"] = "#333333"
        self.txt_fechaPrestamo["justify"] = "center"
        self.txt_fechaPrestamo["text"] = "Entry"
        self.txt_fechaPrestamo["validatecommand"] =self.validate_fechaSolicitud
        self.txt_fechaPrestamo["validate"] ="focus"
        self.txt_fechaPrestamo["textvariable"] = self.fechaPrestamo
        self.txt_fechaPrestamo.place(x=1400,y=125,width=150,height=30)

        self.txt_fechaDevolucion=tk.Entry(frameData)
        self.txt_fechaDevolucion["bg"] = "#d9d9d9"
        self.txt_fechaDevolucion["borderwidth"] = "1px"
        self.txt_fechaDevolucion["font"] = ftEntry
        self.txt_fechaDevolucion["fg"] = "#333333"
        self.txt_fechaDevolucion["justify"] = "center"
        self.txt_fechaDevolucion["validatecommand"] =self.validate_fechaDevolucion
        self.txt_fechaDevolucion["validate"] ="focusout"
        self.txt_fechaDevolucion.place(x=400,y=60,width=150,height=30)

        self.txt_diasRetraso=tk.Entry(frameData)
        self.txt_diasRetraso["bg"] = "#353535"
        self.txt_diasRetraso["borderwidth"] = "1px"
        self.txt_diasRetraso["font"] = ftEntry
        self.txt_diasRetraso["fg"] = "#333333"
        self.txt_diasRetraso["justify"] = "center"
        self.txt_diasRetraso["text"] = "Entry"
        # self.txt_diasRetraso["validatecommand"] =self.validate_idLibroAdd
        # self.txt_diasRetraso["validate"] ="focus"
        self.txt_diasRetraso["textvariable"] = self.diasRestraso
        self.txt_diasRetraso.place(x=1390,y=60,width=150,height=30)

        self.txt_fechaExtravio=tk.Entry(frameData)
        self.txt_fechaExtravio["bg"] = "#353535"
        self.txt_fechaExtravio["borderwidth"] = "1px"
        self.txt_fechaExtravio["font"] = ftEntry
        self.txt_fechaExtravio["fg"] = "#333333"
        self.txt_fechaExtravio["justify"] = "center"
        self.txt_fechaExtravio["text"] = "Entry"
        self.txt_fechaExtravio["validatecommand"] =self.validate_fechaExtravio
        self.txt_fechaExtravio["validate"] ="focus"
        self.txt_fechaExtravio["textvariable"] = self.fechaExtravio
        self.txt_fechaExtravio.place(x=1200,y=115,width=150,height=30)

        ## Mensajes de Error
        self.labelErrorDniSocio = tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelErrorDniSocio["font"] = ft
        self.labelErrorDniSocio["fg"] = "#FF0000"
        self.labelErrorDniSocio["bg"] = "#ffffff"
        self.labelErrorDniSocio["justify"] = "center"
        self.labelErrorDniSocio.place(x=10,y=100,width=150,height=15)

        self.labelErrorCodigoLibro = tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelErrorCodigoLibro["font"] = ft
        self.labelErrorCodigoLibro["fg"] = "#FF0000"
        self.labelErrorCodigoLibro["bg"] = "#ffffff"
        self.labelErrorCodigoLibro["justify"] = "center"
        self.labelErrorCodigoLibro.place(x=200,y=90,width=150,height=15)

        self.labelErrorFechaDevolucion = tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelErrorFechaDevolucion["font"] = ft
        self.labelErrorFechaDevolucion["fg"] = "#FF0000"
        self.labelErrorFechaDevolucion["bg"] = "#ffffff"
        self.labelErrorFechaDevolucion["justify"] = "center"
        self.labelErrorFechaDevolucion.place(x=390,y=90,width=200,height=15)

        self.labelErrorFechaExtravio = tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelErrorFechaExtravio["font"] = ft
        self.labelErrorFechaExtravio["fg"] = "#FF0000"
        self.labelErrorFechaExtravio["bg"] = "#ffffff"
        self.labelErrorFechaExtravio["justify"] = "center"
        self.labelErrorFechaExtravio.place(x=200,y=190,width=150,height=15)

        self.labelErrorFechaSolicitud = tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelErrorFechaSolicitud["font"] = ft
        self.labelErrorFechaSolicitud["fg"] = "#FF0000"
        self.labelErrorFechaSolicitud["bg"] = "#ffffff"
        self.labelErrorFechaSolicitud["justify"] = "center"
        self.labelErrorFechaSolicitud.place(x=390,y=190,width=150,height=15)

        self.labelErrorDiasRetraso = tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelErrorDiasRetraso["font"] = ft
        self.labelErrorDiasRetraso["fg"] = "#FF0000"
        self.labelErrorDiasRetraso["bg"] = "#ffffff"
        self.labelErrorDiasRetraso["justify"] = "center"
        self.labelErrorDiasRetraso.place(x=10,y=255,width=150,height=15)

        self.estadoNuevo = tk.StringVar(frameData)
        self.cb_estadoPrestamo=ttk.Combobox(frameData,values=options,state="readonly",textvariable=self.estadoNuevo)
        self.cb_estadoPrestamo["font"] = ftEntry
        self.cb_estadoPrestamo["justify"] = "center"
        self.cb_estadoBusqueda.set("Eliga Un Estado")
        self.cb_estadoPrestamo.place(x=1000,y=140,width=150,height=30)

        ##boton agregar / modificar
        self.btn_agregarPrestamo=tk.Button(frameData)
        self.btn_agregarPrestamo["bg"] = "#284b63"
        self.btn_agregarPrestamo["font"] = ftButton
        self.btn_agregarPrestamo["fg"] = "#ffffff"
        self.btn_agregarPrestamo["justify"] = "center"
        self.btn_agregarPrestamo["text"] = "Agregar"
        self.btn_agregarPrestamo.place(x=440,y=210,width=110,height=30)
        self.btn_agregarPrestamo["command"] = self.btn_agregarPrestamo_click

    def btn_buscarPrestamo_click(self):
        self.listaPrestamos = []
        self.tablaDatos.delete(*self.tablaDatos.get_children())
        listaPrestamos = self.gestor.BuscarPrestamo(self.codigoBusqueda.get(),self.idLibroBusqueda.get(),self.dniSocioBusqueda.get(), self.estadoBusqueda.get())
        count = 0
        if(len(listaPrestamos)>0):
            for prestamo in listaPrestamos:
                self.tablaDatos.insert(parent="",index="end",iid=count,text="",
                    values=(prestamo[0],prestamo[1],prestamo[2],prestamo[3],prestamo[4], prestamo[5], prestamo[6], prestamo[7]))
                count+=1
            # comment:
            self.frameTable.place(x=10,y=130,width=622,height=210)

    def selectedItemTable(self,event):
        self.prestamoSeleccionado = self.tablaDatos.item(self.tablaDatos.focus()).get("values")
        self.codigoSeleccionado  = self.prestamoSeleccionado[0]

        ## muestro elementos
        self.txt_fechaPrestamo.place(x=400,y=140,width=150,height=30)
        self.txt_fechaExtravio.place(x=200,y=140,width=150,height=30)
        self.txt_diasRetraso.place(x=10,y=220,width=150,height=30)
        self.cb_estadoPrestamo.place(x=10,y=140,width=150,height=30)

        self.set_text_entry(self.prestamoSeleccionado[1], self.txt_socio)
        self.set_text_entry(self.prestamoSeleccionado[2], self.txt_idLibro)
        self.set_text_entry(self.prestamoSeleccionado[3], self.txt_fechaPrestamo)
        self.txt_fechaPrestamo.config(state=tk.DISABLED, disabledforeground="white",disabledbackground="#353535")

        self.set_text_entry(self.prestamoSeleccionado[4], self.txt_fechaDevolucion)
        self.txt_fechaDevolucion.config(state=tk.DISABLED, disabledforeground="white",disabledbackground="#353535")

        self.set_text_entry(self.prestamoSeleccionado[5], self.txt_diasRetraso)
        self.txt_diasRetraso.config(state=tk.DISABLED, disabledforeground="white",disabledbackground="#353535")

        self.set_text_entry(self.prestamoSeleccionado[6], self.txt_fechaExtravio)
        self.txt_fechaExtravio.config(state=tk.DISABLED, disabledforeground="white",disabledbackground="#353535")

        self.cb_estadoPrestamo.set(self.prestamoSeleccionado[7])
        self.btn_agregarPrestamo["text"] = "Modificar"

        ## disable buttons
        self.validate_btn_add()

    def btn_agregarPrestamo_click(self):
        if(self.codigoSeleccionado == ""): ##Agregar Prestamo
            if(self.gestor.agregarPrestamo(self.idLibro.get(), self.dniSocio.get(), self.txt_fechaDevolucion.get())):
                messagebox.showinfo(title="Resultado",message="Se Agrego el prestamo Con exito",parent=self.ventana)
                self.cleanVariables()
            else:
                messagebox.showerror(title="Error",message="Sucedio un Error Al Agregar el prestamo",parent=self.ventana)
        else: ##Modificar Prestamo
            pass
            if(self.gestor.ActualizarPrestamo(self.prestamoSeleccionado[0],self.dniSocio.get(),self.idLibro.get(), self.fechaPrestamo.get(), self.txt_fechaDevolucion.get(), self.diasRestraso.get(), self.fechaExtravio.get(), self.estadoNuevo.get())):
                messagebox.showinfo(title="Resultado",message="Se Actualizo Con exito",parent=self.ventana)
                self.cleanVariables()
            else:
                messagebox.showerror(title="Error",message="Sucedio un Error Al Actualizar el Libro",parent=self.ventana)

    def set_text_entry(self,text,entry):
        entry.delete(0,tk.END)
        entry.insert(0,text)

    def cleanVariables(self):
        self.set_text_entry("",self.txt_idLibro)
        self.set_text_entry("",self.txt_fechaDevolucion)
        self.set_text_entry("",self.txt_socio)
        self.set_text_entry("",self.cant_dias)
        

    ### VALIDACIONES
    def validate_DNISocioAdd(self):
        string =  self.dniSocio.get()
        resultado = False
        if(not self.validate_DNI(string)):
            self.stateEntryDNISocio = False
            self.labelErrorDniSocio.configure(text="Formato Incorrecto")
            resultado = False
        else:
            print('entre else')
            self.stateEntryDNISocio = True
            self.labelErrorDniSocio.configure(text="")
            resultado = True
        self.validate_btn_add()
        return resultado

    def validate_idLibroAdd(self):
        string =  self.idLibro.get()
        resultado = False
        print(f"string: {self.validate_number(string)} {not self.validate_number(string)}")
        if(not self.validate_number(string)):
            self.stateEntryIDlibro = False
            self.labelErrorCodigoLibro.configure(text="Formato Incorrecto")
            resultado = False
        else:
            self.stateEntryIDlibro = True
            self.labelErrorCodigoLibro.configure(text="")
            resultado = True
        self.validate_btn_add()
        return resultado

    def validate_DNISocioSearch(self):
         string =  self.txt_dniSocioBusqueda.get()
         resultado = False
         if(not self.validate_DNI(string)):
             self.stateEntryDNISocioSearch = False
             self.labelWarnigDNI.configure(text="Formato Incorrecto")
             resultado = False
         else:
             print('entre3')
             self.labelWarnigDNI.configure(text="")
             self.stateEntryDNISocioSearch = True
             resultado = True
         self.validate_btn_search()
         return resultado

    def validate_IdLibroSearch(self):
         string =  self.txt_idLibroBusqueda.get()
         resultado = False
         if(not self.validate_number(string)):
             self.stateEntryIDlibroSearch = False
             self.labelWarnigIDLibro.configure(text="Formato Incorrecto")
             resultado = False
         else:
             self.labelWarnigIDLibro.configure(text="")
             self.stateEntryIDlibroSearch = True
             resultado = True
         self.validate_btn_search()
         return resultado

    def validate_fechaDevolucion(self):
        string =  self.txt_fechaDevolucion.get()
        resultado = False
        if(string == '' or  not self.validate_FechaFormat(string)):
            self.stateEntryFechaDevolucion = False
            self.labelErrorFechaDevolucion.configure(text="Formato Incorrecto (dd-mm-aaaa)")
            resultado = False
        elif(not self.validate_fechaDevolucionMayorActual(string)):
            self.stateEntryFechaDevolucion = False
            self.labelErrorFechaDevolucion.configure(text="Debe ser mayor a al fecha actual")
            resultado = False
        else:
            self.stateEntryFechaDevolucion = True
            self.labelErrorFechaDevolucion.configure(text="")
            resultado = True
        self.validate_btn_add()
        return resultado



    def validate_DNI(self, inp):
        patron = re.compile(r'^\d{7,8}$')
        if(inp == ""):
            return True
        if self.validate_number(inp):
            if (patron.match(inp)):
                return True
            else:
                return False
        else:
            return False

    def validate_FechaFormat(self, inp):
        patron = re.compile(r'^(0[1-9]|[1-2][0-9]|3[0-1])-(0[1-9]|1[0-2])-\d{4}$')
        if (patron.match(inp)):
            return True
        else:
            return False

    def validate_fechaDevolucionMayorActual(self, fecha):
        fecha_ingresada = datetime.strptime(fecha, '%d-%m-%Y')
        fecha_actual = datetime.now()
        if fecha_ingresada > fecha_actual:
            return True
        else:
            return False

    def validate_number(self, number):
        if(number.isdigit() or number == ""):
            return True       
        return False

    def validate_fechaExtravio(self):
        string =  self.fechaExtravio.get()
        resultado = False
        print(f"string: {self.valiarFechaExtravioConFechaDervolucion(self.txt_fechaDevolucion.get(), string)}")
        if(string == '' or  not self.validate_FechaFormat(string)):
            self.stateEntryFechaExtravio = False
            self.labelErrorFechaExtravio.configure(text="Formato Incorrecto (dd-mm-aaaa)")
            resultado = False
        if(self.valiarFechaExtravioConFechaDervolucion(self.txt_fechaDevolucion.get(), string)):
            self.stateEntryFechaExtravio = False
            self.labelErrorFechaExtravio.configure(text="Formato Incorrecto")
            resultado = False
        else:
            self.stateEntryFechaExtravio = True
            self.labelErrorFechaExtravio.configure(text="")
            resultado = True
        self.validate_btn_add()
        return resultado

    def validate_fechaSolicitud(self):
        string =  self.fechaPrestamo.get()
        resultado = False
        if(True):
            self.stateEntryFechaSolicitud = False
            self.labelErrorFechaSolicitud.configure(text="Formato Incorrecto")
            resultado = False
        else:
            self.stateEntryFechaSolicitud = True
            self.labelErrorFechaSolicitud.configure(text="")
            resultado = True
        self.validate_btn_add()
        return resultado

    def validate_diasRetraso(self):
        string =  self.diasRestraso.get()
        resultado = False
        # print(f"string: {self.validate_number(string)} {not self.validate_number(string)}")
        if(True):
            self.stateEntryFechaDiasRetraso = False
            self.labelErrorDiasRetraso.configure(text="Formato Incorrecto")
            resultado = False
        else:
            self.stateEntryFechaDiasRetraso = True
            self.labelErrorDiasRetraso.configure(text="")
            resultado = True
        self.validate_btn_add()
        return resultado

    def valiarFechaExtravioConFechaDervolucion(self, fechaDevolucion, fechaIngresada):
        fecha_devolucion_pactada = datetime.strptime(fechaDevolucion, '%d-%m-%Y')
        # Sumar 30 días a la fecha de devolución pactada
        fecha_esperada = fecha_devolucion_pactada + timedelta(days=30)
        # Convertir la fecha ingresada por el usuario a un objeto datetime
        fecha_ingresada = datetime.strptime(fechaIngresada, '%d-%m-%Y')
        # Validar que la fecha ingresada sea exactamente 30 días después de la fecha de devolución pactada
        if fecha_ingresada == fecha_esperada:
            return True
        else:
            return False
    def validate_btn_search(self):
        # print(f'entre validate {self.stateEntryDNI} {self.stateEntryidLibroBusqueda}')
        if(self.stateEntryDNISocioSearch and self.stateEntryIDlibroSearch):
            self.btn_buscarPrestamo.config(state=tk.NORMAL)
        else:
            self.btn_buscarPrestamo.config(state=tk.DISABLED)

    def validate_btn_add(self):
        if(self.stateEntryDNISocio, self.stateEntryIDlibro, self.stateEntryFechaDevolucion, self.stateEntryFechaExtravio, self.stateEntryFechaSolicitud, self.stateEntryFechaDiasRetraso):
            self.btn_agregarPrestamo.config(state=tk.NORMAL)
        else:
           self.btn_agregarPrestamo.config(state=tk.DISABLED)
