import re
from  tkinter import ttk
import tkinter as tk
import tkinter.font as tkFont
from tkinter import messagebox

#import sys
#sys.path.insert(1,'C://Users//Juanola//Desktop//pytondao//')
from Gestores.gestorSocios import GestorSocio

class Window_ManagementMembers:    

    listaSocios = []
    numeroSocioSeleccionado = ""
    estadoSocioSeleccionado = ""
    gestor = GestorSocio()
    validateSearch = True
    stateEntryName = True
    stateEntryLastName = True
    stateEntryDNI = True
    stateEntryNameNew = True
    stateEntryLastNameNew = True
    stateEntryDNINew = True
    
    
    
    def __init__(self, main): 
        self.ventana = tk.Toplevel(main)
        self.ventana.configure(bg='white')
        #setting title
        self.ventana.title("Administracion de Socios")
        self.ventana.grab_set()
        #setting window size
        width=642
        height=602
        screenwidth = self.ventana.winfo_screenwidth()
        screenheight = self.ventana.winfo_screenheight()
        alignstr = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        self.ventana.geometry(alignstr)
        self.ventana.resizable(width=False, height=False)
        
        #Busqueda
        frameSearch = tk.Frame(self.ventana)
        frameSearch["bg"] = "white"
        frameSearch.place(x=10,y=50,width=642,height=100)
               

        #vcmd = self.register(self.validate_String)

        GLabel_594=tk.Label(frameSearch)
        ft = tkFont.Font(family='Trebuchet MS',size=18)
        GLabel_594["font"] = ft
        GLabel_594["fg"] = "#353535"
        GLabel_594["bg"] = "#ffffff"
        GLabel_594["justify"] = "left"
        GLabel_594["text"] = "Nombre"
        GLabel_594.place(x=10,y=0,width=100,height=33)

        GLabel_595=tk.Label(frameSearch)
        ft = tkFont.Font(family='Trebuchet MS',size=18)
        GLabel_595["font"] = ft
        GLabel_595["fg"] = "#353535"
        GLabel_595["bg"] = "#ffffff"
        GLabel_595["justify"] = "left"
        GLabel_595["text"] = "Apellido"
        GLabel_595.place(x=195,y=0,width=100,height=33)

        GLabel_345=tk.Label(frameSearch)
        ft = tkFont.Font(family='Trebuchet MS',size=18)
        GLabel_345["font"] = ft
        GLabel_345["fg"] = "#353535"
        GLabel_345["bg"] = "#ffffff"
        GLabel_345["justify"] = "center"
        GLabel_345["text"] = "Dni"
        GLabel_345.place(x=340,y=0,width=100,height=33)

        self.Apellido = tk.StringVar()
        self.Nombre = tk.StringVar()
        self.DNI = tk.StringVar()
        
        self.labelWarnigName = tk.Label(frameSearch)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigName["font"] = ft
        self.labelWarnigName["fg"] = "#a4161a"
        self.labelWarnigName["bg"] = "#ffffff"
        self.labelWarnigName["justify"] = "center"
        self.labelWarnigName.place(x=20,y=60,width=150,height=22)
        
        
        
        self.txt_Nombre=tk.Entry(frameSearch)
        self.txt_Nombre["bg"] = "#d9d9d9"
        ft = tkFont.Font(family='Trebuchet MS',size=14)
        self.txt_Nombre["font"] = ft
        self.txt_Nombre["fg"] = "#333333"
        self.txt_Nombre["justify"] = "left"
        self.txt_Nombre["validatecommand"] =self.validate_NameSearch
        self.txt_Nombre["validate"] ="focus"
        self.txt_Nombre.place(x=20,y=30,width=150,height=30)
        
        
        
        self.txt_Apellido=tk.Entry(frameSearch)
        self.txt_Apellido["bg"] = "#d9d9d9"
        ft = tkFont.Font(family='Trebuchet MS',size=14)
        self.txt_Apellido["font"] = ft
        self.txt_Apellido["fg"] = "#333333"
        self.txt_Apellido["justify"] = "left"
        self.txt_Apellido["validatecommand"] =self.validate_LastNameSearch
        self.txt_Apellido["validate"] ="focus"
        self.txt_Apellido.place(x=200,y=30,width=150,height=30)
        
        self.labelWarnigLastName = tk.Label(frameSearch)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigLastName["font"] = ft
        self.labelWarnigLastName["fg"] = "#a4161a"
        self.labelWarnigLastName["bg"] = "#ffffff"
        self.labelWarnigLastName["justify"] = "center"
        self.labelWarnigLastName.place(x=200,y=60,width=150,height=22)
        
        
        self.txt_DNI=tk.Entry(frameSearch)
        self.txt_DNI["bg"] = "#d9d9d9"
        ft = tkFont.Font(family='Trebuchet MS',size=14)
        self.txt_DNI["font"] = ft
        self.txt_DNI["fg"] = "#333333"
        self.txt_DNI["justify"] = "left"
        self.txt_DNI["validatecommand"] =self.validate_DNISearch
        self.txt_DNI["validate"] ="focus"        
        self.txt_DNI.place(x=375,y=30,width=150,height=30)
        
        
        self.labelWarnigDNI = tk.Label(frameSearch)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigDNI["font"] = ft
        self.labelWarnigDNI["fg"] = "#a4161a"
        self.labelWarnigDNI["bg"] = "#ffffff"
        self.labelWarnigDNI["justify"] = "center"
        self.labelWarnigDNI.place(x=375,y=60,width=150,height=22)
        

        self.btn_Search=tk.Button(frameSearch)
        self.btn_Search["bg"] = "#284b63"
        ft = tkFont.Font(family='Trebuchet MS',size=16)
        self.btn_Search["font"] = ft
        self.btn_Search["fg"] = "#ffffff"
        self.btn_Search["justify"] = "center"
        self.btn_Search["text"] = "Buscar"
        self.btn_Search["command"] = self.btn_Search_click
        self.btn_Search.place(x=530,y=28,width=80,height=30)
       
        #FIn boton
        
        #TablaSocios
        self.frameTable = tk.Frame(self.ventana)
        self.frameTable["bg"] = "white"
        self.frameTable.place_forget()

        self.estilotabla = ttk.Style(self.frameTable)
        self.estilotabla.configure("Treeview",background="#D9D9D9")
        self.estilotabla.map("Treeview",background = [('selected','#3C6E71')])

        tableScroll = tk.Scrollbar(self.frameTable)
        tableScroll.place(x=602,y = 0,width=20,height=230)
        self.tablaDatos = ttk.Treeview(self.frameTable,yscrollcommand=tableScroll.set)
        tableScroll.config(command=self.tablaDatos.yview)
        
        self.tablaDatos['columns']=('NumeroSocio','Nombre','Apellido','DNI','Genero','Estado') #Definir Columnas
        self.tablaDatos.column("NumeroSocio",width=80,anchor="center",stretch="No")
        self.tablaDatos.column("Nombre",     width=140,anchor="center",stretch="No")
        self.tablaDatos.column("Apellido",   width=140,anchor="center",stretch="No")
        self.tablaDatos.column("DNI",        width=80,anchor="center",stretch="No")
        self.tablaDatos.column("Genero",        width=80,anchor="center",stretch="No")
        self.tablaDatos.column("Estado",        width=80,anchor="center",stretch="No")
        
        #self.tablaDatos.heading("id",         anchor="center",text="")
        self.tablaDatos.heading("NumeroSocio",anchor="center",text="Nroº Socio")
        self.tablaDatos.heading("Nombre",     anchor="center",text="Nombre")
        self.tablaDatos.heading("Apellido",   anchor="center",text="Apellido")
        self.tablaDatos.heading("DNI",        anchor="center",text="DNI")
        self.tablaDatos.heading("Genero",        anchor="center",text="Genero")
        self.tablaDatos.heading("Estado",        anchor="center",text="Estado")
        
        self.tablaDatos['show']='headings'

        # end for
        self.tablaDatos.place(x=0,y=0,width=602,height=230)
        self.tablaDatos.bind("<Double-1>",self.selectedItemTable)

        #FinTablaSocios
        
        frameData = tk.Frame(self.ventana)
        frameData["bg"] =  "white"
        frameData.place(x=10,y=360,width=622,height=240)
        
   
        GLabel_561=tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=18)
        GLabel_561["font"] = ft
        GLabel_561["fg"] = "#353535"
        GLabel_561["justify"] = "left"
        GLabel_561["text"] = "Nombre"
        GLabel_561["bg"] = "#ffffff"
        GLabel_561.place(x=30,y=20,width=100,height=25)

        GLabel_442=tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=18)
        GLabel_442["font"] = ft
        GLabel_442["fg"] = "#353535"
        GLabel_442["justify"] = "left"
        GLabel_442["text"] = "Apellido"
        GLabel_442["bg"] = "#ffffff"
        GLabel_442.place(x=330,y=20,width=100,height=25)

        GLabel_10=tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=18)
        GLabel_10["font"] = ft
        GLabel_10["fg"] = "#353535"
        GLabel_10["justify"] = "left"
        GLabel_10["text"] = "DNI"
        GLabel_10["bg"] = "#ffffff"
        GLabel_10.place(x=330,y=100,width=70,height=25)

        GLabel_800=tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=18)
        GLabel_800["font"] = ft
        GLabel_800["fg"] = "#353535"
        GLabel_800["justify"] = "left"
        GLabel_800["text"] = "Genero"
        GLabel_800["bg"] = "#ffffff"
        GLabel_800.place(x=30,y=100,width=100,height=25)
        

        GLabel_801=tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=18)
        GLabel_801["font"] = ft
        GLabel_801["fg"] = "#353535"
        GLabel_801["justify"] = "left"
        GLabel_801["text"] = "Estado"
        GLabel_801["bg"] = "#ffffff"
        GLabel_801.place(x=30,y=175,width=100,height=25)

        self.generoNuevo = tk.StringVar()
        options=["Femenino","Masculino","Otro"]
        self.cb_Gender = ttk.Combobox(frameData,values=options,state="readonly",textvariable=self.generoNuevo)
        
        ft = tkFont.Font(family='Trebuchet MS',size=14)
        self.cb_Gender["font"] = ft      
        self.cb_Gender.place(x=30,y=130,width=270,height=30)        
        
        self.cb_Gender.set("Elegi un Genero")
        #self.cb_Gender.bind("<<ComboBoxSelected>>",self.cb_Gender_selected)

        
        self.opcionEstado = tk.StringVar()
        optionsEstado=["Activo","Inactivo"]
        self.cb_Estado = ttk.Combobox(frameData,values=optionsEstado,state="readonly",textvariable=self.opcionEstado)
        
        ft = tkFont.Font(family='Trebuchet MS',size=14)
        self.cb_Estado["font"] = ft      
        self.cb_Estado.place(x=30,y=205,width=270,height=30)        
        
        self.cb_Estado.set("Elegi un Estado")
        #self.cb_Gender.bind("<<ComboBoxSelected>>",self.cb_Gender_selected)

        self.opcionGender =tk.IntVar()                
                
        self.nombreNew =  tk.StringVar()
        self.apellidoNew =  tk.StringVar()
        self.DNINew =  tk.StringVar()
        
  
        self.txt_ApellidoNew=tk.Entry(frameData)
        self.txt_ApellidoNew["bg"] = "#d9d9d9"
        ft = tkFont.Font(family='Trebuchet MS',size=14)
        self.txt_ApellidoNew["font"] = ft
        self.txt_ApellidoNew["fg"] = "#333333"
        self.txt_ApellidoNew["justify"] = "left"
        self.txt_ApellidoNew["validatecommand"] =self.validate_LastNameNew
        self.txt_ApellidoNew["validate"] ="focus"        
        self.txt_ApellidoNew.place(x=330,y=50,width=273,height=30)

        self.txt_DNINew=tk.Entry(frameData)
        self.txt_DNINew["bg"] = "#d9d9d9"
        ft = tkFont.Font(family='Trebuchet MS',size=14)
        self.txt_DNINew["font"] = ft
        self.txt_DNINew["fg"] = "#333333"
        self.txt_DNINew["justify"] = "left"
        self.txt_DNINew["validatecommand"] =self.validate_DNINew
        self.txt_DNINew["validate"] ="focus"     
        self.txt_DNINew.place(x=330,y=130,width=271,height=30)           

        self.txt_NombreNew=tk.Entry(frameData)
        self.txt_NombreNew["bg"] = "#d9d9d9"
        ft = tkFont.Font(family='Trebuchet MS',size=14)
        self.txt_NombreNew["font"] = ft
        self.txt_NombreNew["fg"] = "#333333"
        self.txt_NombreNew["justify"] = "left"
        self.txt_NombreNew["validatecommand"] =self.validate_NameNew
        self.txt_NombreNew["validate"] ="focus"
        self.txt_NombreNew.place(x=30,y=50,width=271,height=30) 
        
        self.labelWarnigNombreNew = tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigNombreNew["font"] = ft
        self.labelWarnigNombreNew["fg"] = "#a4161a"
        self.labelWarnigNombreNew["bg"] = "#ffffff"
        self.labelWarnigNombreNew["justify"] = "center"
        self.labelWarnigNombreNew.place(x=30,y=80,width=150,height=22)  

        self.labelWarnigLastNameNew = tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigLastNameNew["font"] = ft
        self.labelWarnigLastNameNew["fg"] = "#a4161a"
        self.labelWarnigLastNameNew["bg"] = "#ffffff"
        self.labelWarnigLastNameNew["justify"] = "center"
        self.labelWarnigLastNameNew.place(x=330,y=80,width=150,height=22) 
        
        
        self.labelWarnigDNINew = tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigDNINew["font"] = ft
        self.labelWarnigDNINew["fg"] = "#a4161a"
        self.labelWarnigDNINew["bg"] = "#ffffff"
        self.labelWarnigDNINew["justify"] = "center"
        self.labelWarnigDNINew.place(x=330,y=160,width=150,height=22) 

        self.btn_addMember=tk.Button(frameData)
        self.btn_addMember["bg"] = "#284b63"
        ft = tkFont.Font(family='Trebuchet MS',size=16)
        self.btn_addMember["font"] = ft
        self.btn_addMember["fg"] = "#ffffff"
        self.btn_addMember["justify"] = "center"
        self.btn_addMember["text"] = "Agregar Cliente"
        self.btn_addMember.place(x=440,y=200,width=183,height=40)
        self.btn_addMember["command"] = self.btn_addMember_click
              

        GLabel_616=tk.Label(self.ventana)
        ft = tkFont.Font(family='Trebuchet MS',size=28)
        GLabel_616["font"] = ft
        GLabel_616["fg"] = "#353535"
        GLabel_616["justify"] = "center"
        GLabel_616["text"] = "Adminsitracion de Socios"
        GLabel_616["bg"] = "#ffffff"
        GLabel_616.place(x=10,y=10,width=614,height=33)

    def btn_Search_click(self): 
        #Agregar Validacion de que no puede venir vacio o obviarlo si viene vacio
        listaSocios = []       
        self.tablaDatos.delete(*self.tablaDatos.get_children())
        #metodo de busqueda de datos 
        listaSocios = self.gestor.BuscarSocio(self.txt_Nombre.get(),self.txt_Apellido.get(),self.txt_DNI.get())
        count = 0
        if(len(listaSocios)>0):
            for socio in listaSocios:
                self.tablaDatos.insert(parent="",index="end",iid=count,text="",
                                values=(socio[0],socio[1],socio[2],socio[3],socio[4],socio[5]))
                count+=1
            # comment: 
            self.frameTable.place(x=10,y=130,width=622,height=240)
        else :
            messagebox.showinfo(title="Resultado",message="No se encontraron Socios",parent=self.ventana)
      
    def btn_addMember_click(self):      
        if(self.numeroSocioSeleccionado == ""):
            if(self.gestor.CrearSocio(self.nombreNew.get(),self.apellidoNew.get(),self.DNINew.get(),self.generoNuevo.get())):
                messagebox.showinfo(title="Resultado",message="Se Creo Con exito",parent=self.ventana)
                self.cleanVariables()
            else:
                messagebox.showerror(title="Error",message="Sucedio un Error Al Crear al Socio",parent=self.ventana)
        else:
            if(self.gestor.ActualizarSocio(self.numeroSocioSeleccionado,self.txt_NombreNew.get(),self.txt_ApellidoNew.get(),self.txt_DNINew.get(),self.cb_Gender.get(),self.cb_Estado.get())):
                messagebox.showinfo(title="Resultado",message="Se Actualizo Con exito",parent=self.ventana)
                self.cleanVariables()
            else:
                messagebox.showerror(title="Error",message="Sucedio un Error Al Actualizar el Socio",parent=self.ventana)

    def selectedItemTable(self,event):
        socioSeleccionado = self.tablaDatos.item(self.tablaDatos.focus()).get("values")
        self.numeroSocioSeleccionado  = socioSeleccionado[0]
        self.set_text_entry(socioSeleccionado[1],self.txt_NombreNew)
        self.set_text_entry(socioSeleccionado[2],self.txt_ApellidoNew)
        self.set_text_entry(socioSeleccionado[3],self.txt_DNINew)
        self.cb_Gender.set(socioSeleccionado[4])
        self.cb_Estado.set(socioSeleccionado[5])
        self.estadoSocioSeleccionado = socioSeleccionado[5]
        self.btn_addMember["text"] = "Modificar Cliente"
        self.validate_btn_add()
    
    def set_text_entry(self,text,entry):
        entry.delete(0,tk.END)
        entry.insert(0,text)
        
    def cleanVariables(self):
        self.set_text_entry("",self.txt_NombreNew)
        self.set_text_entry("",self.txt_ApellidoNew)
        self.set_text_entry("",self.txt_DNINew)
        self.set_text_entry("",self.txt_Nombre)
        self.set_text_entry("",self.txt_Apellido)
        self.set_text_entry("",self.txt_DNI)
        self.tablaDatos.delete(*self.tablaDatos.get_children())
        self.cb_Gender.set("Elegi Un Estado")
        self.cb_Estado.set("Elegi Un Genero")
        self.frameTable.place_forget()

    def validate_String(self,cadena):
        # Expresión regular que busca cualquier dígito (\d) en la cadena
        patron = re.compile(r'^[a-zA-Z]{1,20}$')

        # Verificar la longitud de la cadena

        # Comprobar si la cadena cumple con ambas condiciones
        if patron.match(cadena) or cadena == '':
            return True
        else:
            return False
    

    
    def validate_DNI(self,cadena):
        # Expresión regular que busca cualquier dígito (\d) en la cadena
        patron = re.compile(r'^\d{7,8}$')
        # Verificar la longitud de la cadena
        # Comprobar si la cadena cumple con ambas condiciones
        if patron.match(cadena) or cadena == '':
            return True
        else:
            return False  
        
    
    def validate_NameSearch(self):
        string =  self.txt_Nombre.get()
        resultado = False
        if(not self.validate_String(string)):
            self.stateEntryName = False
            self.labelWarnigName.configure(text="Formato Incorrecto")
            resultado =  False
        else:
            self.labelWarnigName.configure(text="")     
            self.stateEntryName = True 
            resultado = True
        self.validate_btn_search()
        return resultado
    
    def validate_LastNameSearch(self):
        string =  self.txt_Apellido.get()
        resultado = False
        if(not self.validate_String(string)):
            self.stateEntryLastName = False
            self.labelWarnigLastName.configure(text="Formato Incorrecto")
            resultado =  False
        else:
            self.labelWarnigLastName.configure(text="")     
            self.stateEntryLastName = True 
            resultado = True
        self.validate_btn_search()
        return resultado
    
    def validate_DNISearch(self):
        string =  self.txt_DNI.get()
        resultado = False
        if(not self.validate_DNI(string)):
            self.stateEntryDNI = False
            self.labelWarnigDNI.configure(text="Formato Incorrecto")
            resultado = False
        else:
            self.labelWarnigDNI.configure(text="")     
            self.stateEntryDNI = True           
            resultado = True
        self.validate_btn_search()
        return resultado
    
    def validate_btn_search(self):
        if(self.stateEntryName and self.stateEntryLastName and self.stateEntryDNI):
           self.btn_Search.config(state=tk.NORMAL)
        else:
           self.btn_Search.config(state=tk.DISABLED)

    def validate_NameNew(self):
        string =  self.txt_NombreNew.get()
        resultado = False
        if(not self.validate_String(string)):
            self.stateEntryNameNew = False
            self.labelWarnigNombreNew.configure(text="Formato Incorrecto")
            resultado =  False
        else:
            self.labelWarnigNombreNew.configure(text="")     
            self.stateEntryName = True 
            resultado = True
        self.validate_btn_add()
        return resultado
    
    def validate_LastNameNew(self):
        string =  self.txt_ApellidoNew.get()
        resultado = False
        if(not self.validate_String(string)):
            self.stateEntryLastNameNew = False
            self.labelWarnigLastNameNew.configure(text="Formato Incorrecto")
            resultado =  False
        else:
            self.labelWarnigLastNameNew.configure(text="")     
            self.stateEntryLastNameNew = True 
            resultado = True
        self.validate_btn_add()
        return resultado
    
    def validate_DNINew(self):
        string =  self.txt_DNINew.get()
        resultado = False
        if(not self.validate_DNI(string)):
            self.stateEntryDNINew = False
            self.labelWarnigDNINew.configure(text="Formato Incorrecto")
            resultado = False
        else:
            self.labelWarnigDNINew.configure(text="")     
            self.stateEntryDNINew = True           
            resultado = True
        self.validate_btn_add()
        return resultado
    
    def validate_btn_add(self):
        if(self.stateEntryNameNew and self.stateEntryLastNameNew and self.stateEntryDNINew and self.cb_Estado.get() != "Elegi un Estado" and self.cb_Gender != "Elegi un Genero" ):
           self.btn_addMember.config(state=tk.NORMAL)
        else:
           self.btn_addMember.config(state=tk.DISABLED)