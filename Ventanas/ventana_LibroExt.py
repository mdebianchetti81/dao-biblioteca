import re
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import tkinter.font as tkFont
from Gestores.gestorLibros import GestorLibro


class Window_MisplacementBooks:
    
    listaLibrosExtraviados = []
    gestor  = GestorLibro()
    libroSeleccionado = None
    
    stateEntryNameSearch = True
    stateEntryCodeSearch = True
    stateEntryAuthorSearch = True   
    
    
    def __init__(self, main):
        self.ventana =  tk.Toplevel(main)
        self.ventana.configure(bg='white')
        #setting title
        self.ventana.title("Libros Extraviados")
        self.ventana.grab_set()
        #setting window size
        width=642
        height=602
        screenwidth = self.ventana.winfo_screenwidth()
        screenheight = self.ventana.winfo_screenheight()
        alignstr = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        self.ventana.geometry(alignstr)
        self.ventana.resizable(width=False, height=False)
        
        ftLabel = tkFont.Font(family='Trebuchet MS',size=16)
        ftButton = tkFont.Font(family='Trebuchet MS',size=16)
        ftEntry = tkFont.Font(family='Trebuchet MS',size=16)

        
        GLabel_860=tk.Label(self.ventana)
        ft = tkFont.Font(family='Trebuchet MS',size=30)
        GLabel_860["font"] = ft
        GLabel_860["fg"] = "#333333"
        GLabel_860["bg"] = "#ffffff"
        GLabel_860["justify"] = "center"
        GLabel_860["text"] = "Libros Extraviados"
        GLabel_860.place(x=20,y=5,width=592,height=30)
        
        
        frameSearch = tk.Frame(self.ventana)
        frameSearch["bg"] = "white"
        frameSearch.place(x=10,y=40,width=622,height=80)
                

        GLabel_712=tk.Label(frameSearch)
        GLabel_712["font"] = ftLabel
        GLabel_712["fg"] = "#333333"
        GLabel_712["bg"] = "#ffffff"
        GLabel_712["justify"] = "left"
        GLabel_712["text"] = "Codigo"
        GLabel_712.place(x=10,y=0,width=150,height=25)

        GLabel_909=tk.Label(frameSearch)
        GLabel_909["font"] = ftLabel
        GLabel_909["fg"] = "#333333"
        GLabel_909["bg"] = "#ffffff"
        GLabel_909["justify"] = "left"
        GLabel_909["text"] = "Nombre"
        GLabel_909.place(x=180,y=0,width=150,height=25)
                
        GLabel_637=tk.Label(frameSearch)
        GLabel_637["font"] = ftLabel
        GLabel_637["fg"] = "#333333"
        GLabel_637["bg"] = "#ffffff"
        GLabel_637["justify"] = "left"
        GLabel_637["text"] = "Autor"
        GLabel_637.place(x=350,y=0,width=150,height=25)

        self.btn_busquedaLibro=tk.Button(frameSearch)
        self.btn_busquedaLibro["bg"] = "#284b63"
        self.btn_busquedaLibro["font"] = ftButton
        self.btn_busquedaLibro["fg"] = "#ffffff"
        self.btn_busquedaLibro["justify"] = "center"
        self.btn_busquedaLibro["text"] = "Buscar"
        self.btn_busquedaLibro.place(x=520,y=30,width=110,height=30)
        self.btn_busquedaLibro["command"] = self.btn_busquedaLibro_click
        
        self.codigoBusqueda = tk.StringVar()
        self.nombreBusqueda = tk.StringVar()
        self.autorBusqueda = tk.StringVar()

        self.txt_codigoBusqueda=tk.Entry(frameSearch)
        self.txt_codigoBusqueda["bg"] = "#d9d9d9"
        self.txt_codigoBusqueda["borderwidth"] = "1px"
        self.txt_codigoBusqueda["font"] = ftEntry
        self.txt_codigoBusqueda["fg"] = "#070707"
        self.txt_codigoBusqueda["justify"] = "center"
        self.txt_codigoBusqueda["validatecommand"] =self.validate_CodeSearch
        self.txt_codigoBusqueda["validate"] ="focus" 
        self.txt_codigoBusqueda.place(x=10,y=30,width=150,height=30)
        
        self.labelWarnigCodigo = tk.Label(frameSearch)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigCodigo["font"] = ft
        self.labelWarnigCodigo["fg"] = "#a4161a"
        self.labelWarnigCodigo["bg"] = "#ffffff"
        self.labelWarnigCodigo["justify"] = "center"
        self.labelWarnigCodigo.place(x=10,y=60,width=150,height=22)  
            

        self.txt_nombreBusqueda=tk.Entry(frameSearch)
        self.txt_nombreBusqueda["bg"] = "#d9d9d9"
        self.txt_nombreBusqueda["borderwidth"] = "1px"
        self.txt_nombreBusqueda["font"] = ftEntry
        self.txt_nombreBusqueda["fg"] = "#333333"
        self.txt_nombreBusqueda["justify"] = "center"
        self.txt_nombreBusqueda["validatecommand"] =self.validate_NameSearch
        self.txt_nombreBusqueda["validate"] ="focus"
        self.txt_nombreBusqueda.place(x=180,y=30,width=150,height=30)
        
        self.labelWarnigName = tk.Label(frameSearch)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigName["font"] = ft
        self.labelWarnigName["fg"] = "#a4161a"
        self.labelWarnigName["bg"] = "#ffffff"
        self.labelWarnigName["justify"] = "center"
        self.labelWarnigName.place(x=180,y=60,width=150,height=22)
              
        self.txt_autorBusqueda=tk.Entry(frameSearch)
        self.txt_autorBusqueda["bg"] = "#d9d9d9"
        self.txt_autorBusqueda["borderwidth"] = "1px"
        self.txt_autorBusqueda["font"] = ftEntry
        self.txt_autorBusqueda["fg"] = "#333333"
        self.txt_autorBusqueda["justify"] = "center"
        self.txt_autorBusqueda["validatecommand"] =self.validate_AuthorSearch
        self.txt_autorBusqueda["validate"] ="focus"
        self.txt_autorBusqueda.place(x=350,y=30,width=150,height=30)
        
        self.labelWarnigAuthor = tk.Label(frameSearch)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigAuthor["font"] = ft
        self.labelWarnigAuthor["fg"] = "#a4161a"
        self.labelWarnigAuthor["bg"] = "#ffffff"
        self.labelWarnigAuthor["justify"] = "center"
        self.labelWarnigAuthor.place(x=350,y=60,width=150,height=22)
        
        
        self.frameTable = tk.Frame(self.ventana)
        self.frameTable["bg"] = "white"
        self.frameTable.place_forget()
        
        self.estilotabla = ttk.Style(self.frameTable)
        self.estilotabla.configure("Treeview",background="#D9D9D9")
        self.estilotabla.map("Treeview",background = [('selected','#3C6E71')])
                      
        tableScroll = tk.Scrollbar(self.frameTable)
        tableScroll.place(x=602,y = 0,width=20,height=230)
        self.tablaDatos = ttk.Treeview(self.frameTable,yscrollcommand=tableScroll.set)
        tableScroll.config(command=self.tablaDatos.yview)
        self.tablaDatos['columns']=('CodigoLibro','Nombre','Autor','Estado','PrecioReposicion') #Definir Columnas
        self.tablaDatos.column("CodigoLibro",         width=20,anchor="center")
        self.tablaDatos.column("Nombre",         width=160,anchor="center")
        self.tablaDatos.column("Autor",          width=100,anchor="center")
        self.tablaDatos.column("Estado",         width=40,anchor="center")
        self.tablaDatos.column("PrecioReposicion",width=80,anchor="center")
        
        self.tablaDatos.heading("CodigoLibro",            anchor="center",text="Codigo")
        self.tablaDatos.heading("Nombre",            anchor="center",text="Nombre")
        self.tablaDatos.heading("Autor",             anchor="center",text="Autor")
        self.tablaDatos.heading("Estado",            anchor="center",text="Estado")
        self.tablaDatos.heading("PrecioReposicion",  anchor="center",text="Precio Reposicion")
        
        self.tablaDatos['show']='headings'
        
        self.tablaDatos.place(x=0,y=0,width=602,height=230)
        self.tablaDatos.bind("<Double-1>",self.selectedItemTable)
        
        self.frameData = tk.Frame(self.ventana)
        self.frameData["bg"] = "white"
        self.frameData.place(x=10,y=355,width=622,height=210)
        
        
        GLabel_440=tk.Label(self.frameData)
        GLabel_440["font"] = ftLabel
        GLabel_440["fg"] = "#333333"    
        GLabel_440["bg"] = "#ffffff"    
        GLabel_440["justify"] = "left"
        GLabel_440["text"] = "Nombre :"
        GLabel_440.place(x=20,y=0,width=100,height=30)                  
        
        self.lbl_NombreLibro=tk.Label(self.frameData)
        self.lbl_NombreLibro["font"] = ftLabel
        self.lbl_NombreLibro["fg"] = "#333333"    
        self.lbl_NombreLibro["bg"] = "#d9d9d9"    
        self.lbl_NombreLibro["justify"] = "left"
        self.lbl_NombreLibro["text"] = ""
        self.lbl_NombreLibro.place(x=120,y=0,width=190,height=30)            
        
        lbl_TNombreAutor=tk.Label(self.frameData)
        lbl_TNombreAutor["font"] = ftLabel
        lbl_TNombreAutor["fg"] = "#333333"    
        lbl_TNombreAutor["bg"] = "#ffffff"    
        lbl_TNombreAutor["justify"] = "left"
        lbl_TNombreAutor["text"] = "Autor :"
        lbl_TNombreAutor.place(x=20,y=80,width=100,height=30)

        self.lbl_NombreAutor=tk.Label(self.frameData)
        self.lbl_NombreAutor["font"] = ftLabel
        self.lbl_NombreAutor["fg"] = "#333333"    
        self.lbl_NombreAutor["bg"] = "#d9d9d9"    
        self.lbl_NombreAutor["justify"] = "left"
        self.lbl_NombreAutor["text"] = ""
        self.lbl_NombreAutor.place(x=120,y=80,width=190,height=30)
        
        lbl_TPrecioReposicion=tk.Label(self.frameData)
        lbl_TPrecioReposicion["font"] = ftLabel
        lbl_TPrecioReposicion["fg"] = "#333333"    
        lbl_TPrecioReposicion["bg"] = "#ffffff"    
        lbl_TPrecioReposicion["justify"] = "left"
        lbl_TPrecioReposicion["text"] = "Precio Reposicion :"
        lbl_TPrecioReposicion.place(x=330,y=0,width=180,height=30)

        self.lbl_PrecioReposicion=tk.Label(self.frameData)
        self.lbl_PrecioReposicion["font"] = ftLabel
        self.lbl_PrecioReposicion["fg"] = "#333333"    
        self.lbl_PrecioReposicion["bg"] = "#d9d9d9"    
        self.lbl_PrecioReposicion["justify"] = "left"
        self.lbl_PrecioReposicion["text"] = ""
        self.lbl_PrecioReposicion.place(x=520,y=0,width=100,height=30)
    
        lbl_TCodigo=tk.Label(self.frameData)
        lbl_TCodigo["font"] = ftLabel
        lbl_TCodigo["fg"] = "#333333"    
        lbl_TCodigo["bg"] = "#ffffff"    
        lbl_TCodigo["justify"] = "left"
        lbl_TCodigo["text"] = "Codigo :"
        lbl_TCodigo.place(x=320,y=80,width=100,height=30)

        self.lbl_Codigo=tk.Label(self.frameData)
        self.lbl_Codigo["font"] = ftLabel
        self.lbl_Codigo["fg"] = "#333333"    
        self.lbl_Codigo["bg"] = "#d9d9d9"    
        self.lbl_Codigo["justify"] = "left"
        self.lbl_Codigo["text"] = ""
        self.lbl_Codigo.place(x=420,y=80,width=200,height=30)
        
        lbl_TEstado=tk.Label(self.frameData)
        lbl_TEstado["font"] = ftLabel
        lbl_TEstado["fg"] = "#333333"    
        lbl_TEstado["bg"] = "#ffffff"    
        lbl_TEstado["justify"] = "left"
        lbl_TEstado["text"] = "Estado :"
        lbl_TEstado.place(x=20,y=150,width=100,height=30)
        
        self.estadoLibro = tk.StringVar()
      
        options=["Disponible","Prestado","Extraviado"]
        self.cb_estadoLibro=ttk.Combobox(self.frameData,values=options,state="readonly",textvariable=self.estadoLibro)
        self.cb_estadoLibro["font"] = ftEntry
        self.cb_estadoLibro.set("Eliga Un Estado")
        self.cb_estadoLibro.place(x=120,y=150,width=190,height=30)
        
        self.btn_agregarLibro=tk.Button(self.frameData)
        self.btn_agregarLibro["bg"] = "#284b63"
        self.btn_agregarLibro["font"] = ftButton
        self.btn_agregarLibro["fg"] = "#ffffff"
        self.btn_agregarLibro["justify"] = "center"
        self.btn_agregarLibro["text"] = "Actualizar Libro"
        self.btn_agregarLibro.place(x=420,y=150,width=200,height=30)
        self.btn_agregarLibro["command"] = self.btn_modificar_click        
        
        
        frameBottom = tk.Frame(self.ventana)
        frameBottom["bg"] = "#d9d9d9"
        frameBottom.place(x=10,y=570,width=622,height=30)

        GLabel_446=tk.Label(frameBottom)
        GLabel_446["font"] = ftLabel
        GLabel_446["fg"] = "#333333"    
        GLabel_446["bg"] = "#d9d9d9"    
        GLabel_446["justify"] = "left"
        GLabel_446["text"] = "Cantidad De Libros Extraviados:"
        GLabel_446.place(x=10,y=0,width=333,height=30)

        self.lbl_cantidadRegistro=tk.Label(frameBottom)
        self.lbl_cantidadRegistro["font"] = ftLabel
        self.lbl_cantidadRegistro["fg"] = "#333333"
        self.lbl_cantidadRegistro["bg"] = "#d9d9d9"
        self.lbl_cantidadRegistro["justify"] = "center"
        self.lbl_cantidadRegistro["text"] = ""
        self.lbl_cantidadRegistro.place(x=340,y=0,width=70,height=30)
        
        
        

    def btn_busquedaLibro_click(self):
        self.listaLibrosExtraviados = []
        self.tablaDatos.delete(*self.tablaDatos.get_children())      
        listaLibrosExtraviados = self.gestor.BuscarLibrosExtraviados(self.codigoBusqueda.get(),self.nombreBusqueda.get(),self.autorBusqueda.get())
        count = 0
        if(len(listaLibrosExtraviados)>0):
            for libro in listaLibrosExtraviados:
                self.tablaDatos.insert(parent="",index="end",iid=count,text="",
                    values=(libro[0],libro[1],libro[2],libro[3],libro[4]))
                count+=1
            # comment: 
            self.frameTable.place(x=10,y=120,width=622,height=230)
            self.lbl_cantidadRegistro["text"] = str(len(listaLibrosExtraviados))
        else :
            messagebox.showinfo(title="Resultado",message="No se encontraron Libros extraviados",parent=self.ventana)


    def selectedItemTable(self,event):
        self.libroSeleccionado = self.tablaDatos.item(self.tablaDatos.focus()).get("values")
        self.lbl_Codigo["text"] = (self.libroSeleccionado[0])
        self.lbl_NombreLibro["text"] =(self.libroSeleccionado[1])
        self.lbl_NombreAutor["text"] =(self.libroSeleccionado[2])
        self.lbl_PrecioReposicion["text"] = f"$ {(self.libroSeleccionado[4])}" 
        self.cb_estadoLibro.set(self.libroSeleccionado[3])    
                          
        
    def btn_modificar_click(self):
        if(self.gestor.ActualizarLibro(self.libroSeleccionado[0],self.libroSeleccionado[1],self.libroSeleccionado[2],self.libroSeleccionado[4],self.cb_estadoLibro.get())):
            messagebox.showinfo(title="Resultado",message="Se Actualizo Con exito",parent=self.ventana)
            self.cleanVariables()
        else:
            messagebox.showerror(title="Error",message="Sucedio un Error Al Actualizar el Libro",parent=self.ventana)
    
    def cleanVariables(self):
        self.lbl_Codigo["text"] = ""
        self.lbl_NombreLibro["text"] =""
        self.lbl_NombreAutor["text"] =""
        self.lbl_PrecioReposicion["text"] = ""
        self.cb_estadoLibro.set("Eliga Un Estado")
        self.tablaDatos.delete(*self.tablaDatos.get_children())   

    def validate_NameSearch(self):
        string =  self.txt_nombreBusqueda.get()
        resultado = False
        if(not self.validate_String(string)):
            self.stateEntryNameSearch = False
            self.labelWarnigName.configure(text="Formato Incorrecto")
            resultado =  False
        else:
            self.labelWarnigName.configure(text="")     
            self.stateEntryNameSearch = True 
            resultado = True
        self.validate_btn_search()
        return resultado
    
    def validate_CodeSearch(self):
        string =  self.txt_codigoBusqueda.get()
        resultado = False
        if(not self.validate_Code(string)):
            self.stateEntryCodeSearch = False
            self.labelWarnigCodigo.configure(text="Formato Incorrecto")
            resultado =  False
        else:
            self.labelWarnigCodigo.configure(text="")     
            self.stateEntryCodeSearch = True 
            resultado = True
        self.validate_btn_search()
        return resultado

    def validate_AuthorSearch(self):
        string =  self.txt_autorBusqueda.get()
        resultado = False
        if(not self.validate_String(string)):
            self.stateEntryAuthorSearch = False
            self.labelWarnigAuthor.configure(text="Formato Incorrecto")
            resultado =  False
        else:
            self.labelWarnigAuthor.configure(text="")     
            self.stateEntryAuthorSearch = True 
            resultado = True
        self.validate_btn_search()
        return resultado
    
    def validate_btn_search(self):
        if(self.stateEntryNameSearch and self.stateEntryAuthorSearch and self.stateEntryCodeSearch):
           self.btn_busquedaLibro.config(state=tk.NORMAL)
        else:
           self.btn_busquedaLibro.config(state=tk.DISABLED)
    
    
    
    def validate_String(self,cadena):
        # Expresión regular que busca cualquier dígito (\d) en la cadena
        patron = re.compile(r'^[a-zA-Z]{1,50}$')

        # Verificar la longitud de la cadena

        # Comprobar si la cadena cumple con ambas condiciones
        if patron.match(cadena) or cadena == '':
            return True
        else:
            return False
    
    def validate_Code(self,cadena):
        if cadena.isdigit() or cadena == '':
            return True
        else:
            return False
    