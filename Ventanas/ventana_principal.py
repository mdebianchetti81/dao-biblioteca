from datetime import datetime

import locale
import tkinter as tk
from Ventanas.ventana_AdmSocio import Window_ManagementMembers
from Ventanas.ventana_AdmLibro import Window_ManagementBooks
from Ventanas.ventana_LibroExt import Window_MisplacementBooks
from Ventanas.ventana_AdmPrestamo import Window_ManagementLoans
from Ventanas.ventana_AdmDevolucion import Window_ManagementRefund
from Ventanas.ventana_Reportes import Window_Reports
from PIL import ImageTk
from PIL import Image
import tkinter.font as tkFont

class Windows_Main:

    
    def __init__(self):
        
        self.ventana = tk.Tk()       
        #setting title
        self.ventana.title("BiblioManager")
        #setting background
        self.ventana.configure(bg='white')
        #setting window size
        width=642
        height=602
        screenwidth = self.ventana.winfo_screenwidth()
        screenheight = self.ventana.winfo_screenheight()
        alignstr = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        self.ventana.geometry(alignstr)
        self.ventana.resizable(width=False, height=False)
        
        mainframe = tk.Frame()
        mainframe.pack()
        
        btn_ManagementMembers=tk.Button(self.ventana)
        btn_ManagementMembers["bg"] = "#284b63"
        ft = tkFont.Font(family='Trebuchet MS',size=30)
        btn_ManagementMembers["font"] = ft
        btn_ManagementMembers["fg"] = "#ffffff"
        btn_ManagementMembers["justify"] = "center"
        btn_ManagementMembers["text"] = "Administrar\nSocios"
        btn_ManagementMembers.place(x=30,y=90,width=265,height=150)
        btn_ManagementMembers["command"] = self.btn_ManagementMembers_click

        btn_ManagementBooks=tk.Button(self.ventana)
        btn_ManagementBooks["bg"] = "#284b63"
        btn_ManagementBooks["font"] = ft
        btn_ManagementBooks["fg"] = "#ffffff"
        btn_ManagementBooks["justify"] = "center"
        btn_ManagementBooks["text"] = "Administrar\nLibros"
        btn_ManagementBooks.place(x=340,y=90,width=265,height=150)
        btn_ManagementBooks["command"] = self.btn_ManagementBooks_command

        btn_LoansRefund=tk.Button(self.ventana)
        btn_LoansRefund["bg"] = "#284b63"
        btn_LoansRefund["font"] = ft
        btn_LoansRefund["fg"] = "#ffffff"
        btn_LoansRefund["justify"] = "center"
        btn_LoansRefund["text"] = "Prestamos"
        btn_LoansRefund.place(x=30,y=290,width=265,height=150)
        btn_LoansRefund["command"] = self.btn_LoansRefund_command


        # btn_Refund=tk.Button(self.ventana)
        # btn_Refund["bg"] = "#284b63"
        # btn_Refund["font"] = ft
        # btn_Refund["fg"] = "#ffffff"
        # btn_Refund["justify"] = "center"
        # btn_Refund["text"] = "Devoluciones"
        # btn_Refund.place(x=30,y=390,width=265,height=50)
        # btn_Refund["command"] = self.btn_Refund_command

        btn_MisplacementBooks=tk.Button(self.ventana)
        btn_MisplacementBooks["bg"] = "#284b63"
        btn_MisplacementBooks["font"] = ft
        btn_MisplacementBooks["fg"] = "#ffffff"
        btn_MisplacementBooks["justify"] = "center"
        btn_MisplacementBooks["text"] = "Extravios"
        btn_MisplacementBooks.place(x=340,y=290,width=265,height=150)
        btn_MisplacementBooks["command"] = self.btn_MisplacementBooks_command

        btn_Reports=tk.Button(self.ventana)
        btn_Reports["bg"] = "#284b63"
        btn_Reports["font"] = ft
        btn_Reports["fg"] = "#ffffff"
        btn_Reports["justify"] = "center"
        btn_Reports["text"] = "Reportes"
        btn_Reports.place(x=30,y=490,width=577,height=63)
        btn_Reports["command"] = self.btn_Reports_command

        GLabel_424=tk.Label(self.ventana)
        ft = tkFont.Font(family='Trebuchet MS',size=20)
        GLabel_424["font"] = ft
        GLabel_424["fg"] = "#353535"
        GLabel_424["bg"] = "#ffffff"
        GLabel_424["justify"] = "center"
        GLabel_424["text"] = "BiblioManager"
        GLabel_424.place(x=30,y=20,width=170,height=46)

        GLabel_418=tk.Label(self.ventana)
        ft = tkFont.Font(family='Trebuchet MS',size=16)
        GLabel_418["font"] = ft
        GLabel_418["fg"] = "#333333"
        GLabel_418["bg"] = "#ffffff"
        GLabel_418["justify"] = "right"
        GLabel_418["text"] = self.obtenerFechaHoy()
        GLabel_418.place(x=375,y=30,width=235,height=30)
        
        logo = ImageTk.PhotoImage(Image.open("./Recursos/Library.png").resize((50,40)))
        panel = tk.Label(self.ventana, image=logo)
        panel.image = logo
        panel.pack(pady=2)
        panel.config(bg="white")
        panel.place(x=200,y=20)
         
        self.popup = None

    def btn_ManagementMembers_click(self):
        self.open_up(Window_ManagementMembers(self.ventana))
    def btn_ManagementBooks_command(self):
        self.open_up(Window_ManagementBooks(self.ventana))

    def btn_LoansRefund_command(self):
        # self.ventanaPrestamosDevoluciones  = "" #Crear la self.ventana
        self.open_up(Window_ManagementLoans(self.ventana))
    
    def btn_Refund_command(self):
        # self.ventanaPrestamosDevoluciones  = "" #Crear la self.ventana
        self.open_up(Window_ManagementRefund(self.ventana))

    def btn_MisplacementBooks_command(self):
        self.open_up(Window_MisplacementBooks(self.ventana))
        
    def btn_Reports_command(self):
        
        self.open_up(Window_Reports(self.ventana))
                
    def obtenerFechaHoy(self):
        locale.setlocale(locale.LC_TIME,'es_AR.UTF-8')    
        fecha = datetime.now()
        return fecha.strftime("%d de %B de %Y")
    
    def open_up(self,window):
        if self.popup is None or not self.popup.top.winfo_exists():
            self.popup = window
        else:
            self.popup.top.lift(self.master)


    def mostrar(self):
        self.ventana.mainloop()

