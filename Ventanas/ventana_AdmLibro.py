import re
import tkinter as tk
from tkinter import ttk
from tkinter import messagebox
import tkinter.font as tkFont
from Gestores.gestorLibros import GestorLibro

class Window_ManagementBooks:
    
    listaLibros = []
    numeroLibroSeleccionado = ""
    estadoLibroSeleccionado = ""
    stateEntryName = True
    stateEntryCodigo = True
    
    stateEntryCodigoAdd = True
    stateEntryNombreAdd  = True
    stateEntryAutorAdd = True
    stateEntryPrecioAdd = True
    
    gestor = GestorLibro()
    
    
    def __init__(self, main):
        self.ventana =  tk.Toplevel(main)
        self.ventana.configure(bg='white')
        #setting title
        self.ventana.title("Adminsitracion de Libros")
        self.ventana.grab_set()
        #setting window size
        width=642
        height=602
        screenwidth = self.ventana.winfo_screenwidth()
        screenheight = self.ventana.winfo_screenheight()
        alignstr = '%dx%d+%d+%d' % (width, height, (screenwidth - width) / 2, (screenheight - height) / 2)
        self.ventana.geometry(alignstr)
        self.ventana.resizable(width=False, height=False)
        
        ftLabel = tkFont.Font(family='Trebuchet MS',size=16)
        ftButton = tkFont.Font(family='Trebuchet MS',size=16)
        ftEntry = tkFont.Font(family='Trebuchet MS',size=16)

        
        
        GLabel_860=tk.Label(self.ventana)
        ft = tkFont.Font(family='Trebuchet MS',size=30)
        GLabel_860["font"] = ft
        GLabel_860["fg"] = "#333333"
        GLabel_860["bg"] = "#ffffff"
        GLabel_860["justify"] = "center"
        GLabel_860["text"] = "Administración de Libros"
        GLabel_860.place(x=20,y=10,width=592,height=30)


        #--------------------
        frameSearch = tk.Frame(self.ventana)
        frameSearch["bg"] = "#ffffff"
        frameSearch.place(x=10,y=50,width=622,height=90)            
        
        GLabel_712=tk.Label(frameSearch)
        GLabel_712["font"] = ftLabel
        GLabel_712["fg"] = "#333333"
        GLabel_712["bg"] = "#ffffff"
        GLabel_712["justify"] = "left"
        GLabel_712["text"] = "Codigo"
        GLabel_712.place(x=10,y=0,width=150,height=25)

        GLabel_909=tk.Label(frameSearch)
        GLabel_909["font"] = ftLabel
        GLabel_909["fg"] = "#333333"
        GLabel_909["justify"] = "left"
        GLabel_909["bg"] = "#ffffff"
        GLabel_909["text"] = "Nombre"
        GLabel_909.place(x=180,y=0,width=150,height=25)

        GLabel_448=tk.Label(frameSearch)
        GLabel_448["font"] = ftLabel
        GLabel_448["fg"] = "#333333"
        GLabel_448["justify"] = "left"
        GLabel_448["bg"] = "#ffffff"
        GLabel_448["text"] = "Estado"
        GLabel_448.place(x=350,y=0,width=150,height=25)
       
        self.btn_buscarLibro=tk.Button(frameSearch)
        self.btn_buscarLibro["bg"] = "#284b63"
        self.btn_buscarLibro["font"] = ftButton
        self.btn_buscarLibro["fg"] = "#ffffff"
        self.btn_buscarLibro["justify"] = "center"
        self.btn_buscarLibro["text"] = "Buscar"
        self.btn_buscarLibro.place(x=520,y=30,width=110,height=30)
        self.btn_buscarLibro["command"] = self.btn_buscarLibro_click     
        
        self.codigoBusqueda = tk.StringVar()
        self.nombreBusqueda = tk.StringVar()
        self.estadoBusqueda = tk.StringVar()
        
        self.txt_codigoBusqueda=tk.Entry(frameSearch)
        self.txt_codigoBusqueda["bg"] = "#d9d9d9"
        self.txt_codigoBusqueda["font"] = ftEntry
        self.txt_codigoBusqueda["fg"] = "#070707"
        self.txt_codigoBusqueda["justify"] = "center"
        self.txt_codigoBusqueda["validatecommand"] =self.validate_CodeSearch
        self.txt_codigoBusqueda["validate"] ="focus"      
        self.txt_codigoBusqueda.place(x=10,y=30,width=150,height=30)
 
        self.labelWarnigCodigo = tk.Label(frameSearch)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigCodigo["font"] = ft
        self.labelWarnigCodigo["fg"] = "#a4161a"
        self.labelWarnigCodigo["bg"] = "#ffffff"
        self.labelWarnigCodigo["justify"] = "center"
        self.labelWarnigCodigo.place(x=10,y=60,width=150,height=22)  
            

        self.txt_nombreBusqueda=tk.Entry(frameSearch)
        self.txt_nombreBusqueda["bg"] = "#d9d9d9"
        self.txt_nombreBusqueda["font"] = ftEntry
        self.txt_nombreBusqueda["fg"] = "#333333"
        self.txt_nombreBusqueda["justify"] = "center"
        self.txt_nombreBusqueda["validatecommand"] =self.validate_NameSearch
        self.txt_nombreBusqueda["validate"] ="focus"
        self.txt_nombreBusqueda.place(x=180,y=30,width=150,height=30)
        
        self.labelWarnigName = tk.Label(frameSearch)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigName["font"] = ft
        self.labelWarnigName["fg"] = "#a4161a"
        self.labelWarnigName["bg"] = "#ffffff"
        self.labelWarnigName["justify"] = "center"
        self.labelWarnigName.place(x=180,y=60,width=150,height=22)
        
        
        options=["Disponible","Prestado","Extraviado"]
        self.cb_estadoBusqueda=ttk.Combobox(frameSearch,values=options,state="readonly",textvariable=self.estadoBusqueda)
        self.cb_estadoBusqueda["font"] = ftEntry
        self.cb_estadoBusqueda.set("Eliga Un Estado")
        self.cb_estadoBusqueda.place(x=350,y=30,width=150,height=30)


        #-----------------------
        self.frameTable= tk.Frame(self.ventana)
        self.frameTable["bg"] = "white"
        self.frameTable.place_forget()
        
        self.estilotabla = ttk.Style(self.frameTable)
        self.estilotabla.configure("Treeview",background="#D9D9D9")
        self.estilotabla.map("Treeview",background = [('selected','#3C6E71')])

        tableScroll = tk.Scrollbar(self.frameTable)
        tableScroll.place(x=602,y = 0,width=20,height=230)
        self.tablaDatos = ttk.Treeview(self.frameTable,yscrollcommand=tableScroll.set)
        tableScroll.config(command=self.tablaDatos.yview)
        self.tablaDatos['columns']=('CodigoLibro','Nombre','Autor','Estado','PrecioReposicion') #Definir Columnas
        self.tablaDatos.column("CodigoLibro",         width=60,anchor="center",stretch=tk.NO)
        self.tablaDatos.column("Nombre",         width=180,anchor="center",stretch=tk.NO)
        self.tablaDatos.column("Autor",          width=180,anchor="center",stretch=tk.NO)
        self.tablaDatos.column("Estado",         width=75,anchor="center",stretch=tk.NO)
        self.tablaDatos.column("PrecioReposicion",width=103,anchor="center",stretch=tk.NO)
        
        self.tablaDatos.heading("CodigoLibro",            anchor="center",text="Codigo")
        self.tablaDatos.heading("Nombre",            anchor="center",text="Nombre")
        self.tablaDatos.heading("Autor",             anchor="center",text="Autor")
        self.tablaDatos.heading("Estado",            anchor="center",text="Estado")
        self.tablaDatos.heading("PrecioReposicion",  anchor="center",text="Precio Reposicion")
        
        self.tablaDatos['show']='headings'

        # end for
        self.tablaDatos.place(x=0,y=0,width=602,height=230)
        self.tablaDatos.bind("<Double-1>",self.selectedItemTable)
        #----------------

        frameData= tk.Frame(self.ventana)
        frameData["bg"] = "white"
        frameData.place(x=10,y=360,width=622,height=260)

    
        ftLabel = tkFont.Font(family='Trebuchet MS',size=16)
        ftButton = tkFont.Font(family='Trebuchet MS',size=16)
        ftEntry = tkFont.Font(family='Trebuchet MS',size=16)


        GLabel_936=tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=20)
        GLabel_936["font"] = ft
        GLabel_936["fg"] = "#333333"
        GLabel_936["bg"] = "#ffffff"
        GLabel_936["justify"] = "center"
        GLabel_936["text"] = "Agregar Libro"
        GLabel_936.place(x=20,y=-5,width=184,height=35)


        GLabel_446=tk.Label(frameData)
        GLabel_446["font"] = ftLabel
        GLabel_446["fg"] = "#333333"
        GLabel_446["bg"] = "#ffffff"
        GLabel_446["justify"] = "left"
        GLabel_446["text"] = "Codigo"
        GLabel_446.place(x=10,y=30,width=150,height=25)
        
    

        GLabel_102=tk.Label(frameData)
        GLabel_102["font"] = ftLabel
        GLabel_102["fg"] = "#333333"
        GLabel_102["bg"] = "#ffffff"
        GLabel_102["justify"] = "left"
        GLabel_102["text"] = "Nombre"
        GLabel_102.place(x=200,y=30,width=150,height=25)
        
        
        
        GLabel_466=tk.Label(frameData)
        GLabel_466["font"] = ftLabel
        GLabel_466["fg"] = "#333333"
        GLabel_466["bg"] = "#ffffff"
        GLabel_466["justify"] = "left"
        GLabel_466["text"] = "Autor"
        GLabel_466.place(x=390,y=30,width=150,height=25)


        GLabel_78=tk.Label(frameData)
        GLabel_78["font"] = ftLabel
        GLabel_78["fg"] = "#333333"
        GLabel_78["bg"] = "#ffffff"
        GLabel_78["justify"] = "left"
        GLabel_78["text"] = "Estado"
        GLabel_78.place(x=10,y=115,width=150,height=25)

      
        GLabel_333=tk.Label(frameData)
        GLabel_333["font"] = ftLabel
        GLabel_333["fg"] = "#333333"
        GLabel_333["bg"] = "#ffffff"
        GLabel_333["justify"] = "center"
        GLabel_333["text"] = "Precio de Reposicion"
        GLabel_333.place(x=200,y=115,width=200,height=30)
        
        self.codigoLibro = tk.StringVar()
        self.nombreLibro = tk.StringVar()
        self.autorLibro = tk.StringVar()
        self.estadoLibro = tk.StringVar()
        self.precioLibro = tk.DoubleVar()

        self.txt_codigoLibro=tk.Entry(frameData)
        self.txt_codigoLibro["bg"] = "#d9d9d9"
        self.txt_codigoLibro["borderwidth"] = "1px"
        self.txt_codigoLibro["font"] = ftEntry
        self.txt_codigoLibro["fg"] = "#333333"
        self.txt_codigoLibro["justify"] = "center"
        self.txt_codigoLibro["validatecommand"] =self.validate_CodeAdd
        self.txt_codigoLibro["validate"] ="focus"      
        self.txt_codigoLibro.place(x=10,y=60,width=150,height=30)
        
        self.labelWarnigCodeAdd = tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigCodeAdd["font"] = ft
        self.labelWarnigCodeAdd["fg"] = "#a4161a"
        self.labelWarnigCodeAdd["bg"] = "#ffffff"
        self.labelWarnigCodeAdd["justify"] = "center"
        self.labelWarnigCodeAdd.place(x=10,y=90,width=150,height=22)

        self.txt_nombreLibro=tk.Entry(frameData)
        self.txt_nombreLibro["bg"] = "#d9d9d9"
        self.txt_nombreLibro["borderwidth"] = "1px"
        self.txt_nombreLibro["font"] = ftEntry
        self.txt_nombreLibro["fg"] = "#333333"
        self.txt_nombreLibro["justify"] = "center"
        self.txt_nombreLibro["validatecommand"] =self.validate_NameAdd
        self.txt_nombreLibro["validate"] ="focus" 
        self.txt_nombreLibro.place(x=200,y=60,width=150,height=30)
        
        self.labelWarnigNameAdd = tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigNameAdd["font"] = ft
        self.labelWarnigNameAdd["fg"] = "#a4161a"
        self.labelWarnigNameAdd["bg"] = "#ffffff"
        self.labelWarnigNameAdd["justify"] = "center"
        self.labelWarnigNameAdd.place(x=200,y=90,width=150,height=22)

        self.txt_autorLibro=tk.Entry(frameData)
        self.txt_autorLibro["bg"] = "#d9d9d9"
        self.txt_autorLibro["borderwidth"] = "1px"
        self.txt_autorLibro["font"] = ftEntry
        self.txt_autorLibro["fg"] = "#333333"
        self.txt_autorLibro["justify"] = "center"
        self.txt_autorLibro["validatecommand"] =self.validate_AuthorAdd
        self.txt_autorLibro["validate"] ="focus" 
        self.txt_autorLibro.place(x=390,y=60,width=150,height=30)
        
        self.labelWarnigAuthorAdd = tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigAuthorAdd["font"] = ft
        self.labelWarnigAuthorAdd["fg"] = "#a4161a"
        self.labelWarnigAuthorAdd["bg"] = "#ffffff"
        self.labelWarnigAuthorAdd["justify"] = "center"
        self.labelWarnigAuthorAdd.place(x=390,y=90,width=150,height=22)

        self.txt_precioLibro=tk.Entry(frameData)
        self.txt_precioLibro["bg"] = "#d9d9d9"
        self.txt_precioLibro["borderwidth"] = "1px"
        self.txt_precioLibro["font"] = ftEntry
        self.txt_precioLibro["fg"] = "#333333"
        self.txt_precioLibro["justify"] = "center"
        self.txt_precioLibro["validatecommand"] =self.validate_PricingAdd
        self.txt_precioLibro["validate"] ="focus" 
        self.txt_precioLibro.place(x=200,y=145,width=150,height=30)
        
        self.labelWarnigPricingAdd = tk.Label(frameData)
        ft = tkFont.Font(family='Trebuchet MS',size=8)
        self.labelWarnigPricingAdd["font"] = ft
        self.labelWarnigPricingAdd["fg"] = "#a4161a"
        self.labelWarnigPricingAdd["bg"] = "#ffffff"
        self.labelWarnigPricingAdd["justify"] = "center"
        self.labelWarnigPricingAdd.place(x=200,y=175,width=150,height=22)
        
        self.estadoNuevo = tk.StringVar()

        self.cb_estadoLibro=ttk.Combobox(frameData,values=options,state="readonly",textvariable=self.estadoNuevo)
        self.cb_estadoLibro["font"] = ftEntry
        self.cb_estadoLibro["justify"] = "center"
        self.cb_estadoBusqueda.set("Eliga Un Estado")
        self.cb_estadoLibro.place(x=10,y=145,width=150,height=30)

        self.btn_agregarLibro=tk.Button(frameData)
        self.btn_agregarLibro["bg"] = "#284b63"
        self.btn_agregarLibro["font"] = ftButton
        self.btn_agregarLibro["fg"] = "#ffffff"
        self.btn_agregarLibro["justify"] = "center"
        self.btn_agregarLibro["text"] = "Agregar"
        self.btn_agregarLibro.place(x=440,y=210,width=183,height=30)
        self.btn_agregarLibro["command"] = self.btn_agregarLibro_click

    def btn_buscarLibro_click(self):
        self.listaLibros = []
        self.tablaDatos.delete(*self.tablaDatos.get_children())
        listaLibros = self.gestor.BuscarLibro(self.txt_codigoBusqueda.get(),self.txt_nombreBusqueda.get(),self.cb_estadoBusqueda.get())
        count = 0
        if(len(listaLibros)>0):
            for libro in listaLibros:
                self.tablaDatos.insert(parent="",index="end",iid=count,text="",
                    values=(libro[0],libro[1],libro[2],libro[3],libro[4]))
                count+=1
            # comment: 
            self.frameTable.place(x=10,y=130,width=622,height=230)
        else :
            messagebox.showinfo(title="Resultado",message="No se encontraron Socios",parent=self.ventana)
      

    def btn_agregarLibro_click(self):
        precio = self.precioLibro.get()
        print("Codigo " + self.codigoLibro.get())
        print("Nombre " +self.nombreLibro.get())
        print("Autor " +self.autorLibro.get())
        print("Estado " +self.estadoLibro.get())
        print("Precio " +str(precio))
        
        
        if(self.numeroLibroSeleccionado == ""):
            if(self.gestor.CrearLibro(self.nombreLibro.get(),self.autorLibro.get(),precio)):
                messagebox.showinfo(title="Resultado",message="Se Creo Con exito",parent=self.ventana)
                self.cleanVariables()
            else:
                messagebox.showerror(title="Error",message="Sucedio un Error Al Crear el Libro",parent=self.ventana)
    
        else:
            if(self.gestor.ActualizarLibro(self.codigoLibro.get(),self.nombreLibro.get(),self.autorLibro.get(),precio,self.cb_estadoLibro.get())):
                messagebox.showinfo(title="Resultado",message="Se Actualizo Con exito",parent=self.ventana)
                self.cleanVariables()
            else:
                messagebox.showerror(title="Error",message="Sucedio un Error Al Actualizar el Libro",parent=self.ventana)


    def selectedItemTable(self,event):
        libroSeleccionado = self.tablaDatos.item(self.tablaDatos.focus()).get("values")
        self.numeroLibroSeleccionado  = libroSeleccionado[0]
        self.set_text_entry(libroSeleccionado[0],self.txt_codigoLibro)
        self.set_text_entry(libroSeleccionado[1],self.txt_nombreLibro)
        self.set_text_entry(libroSeleccionado[2],self.txt_autorLibro)
        self.set_text_entry(libroSeleccionado[4],self.txt_precioLibro)
        self.cb_estadoLibro.set(libroSeleccionado[3])
        self.estadoLibroSeleccionado = libroSeleccionado[3]
        self.btn_agregarLibro["text"] = "Modificar Libro"
        
    def set_text_entry(self,text,entry):
        entry.delete(0,tk.END)
        entry.insert(0,text)
        
    def cleanVariables(self):
        self.set_text_entry("",self.txt_codigoLibro)
        self.set_text_entry("",self.txt_nombreLibro)
        self.set_text_entry("",self.txt_autorLibro)
        self.set_text_entry("",self.txt_precioLibro)
        self.set_text_entry("",self.txt_codigoBusqueda)
        self.set_text_entry("",self.txt_nombreBusqueda)
        self.tablaDatos.delete(*self.tablaDatos.get_children())
        self.cb_estadoBusqueda.set("Eliga Un Estado")
        self.cb_estadoLibro.set("Eliga Un Estado")
        
    def validate_String(self,cadena):
        # Expresión regular que busca cualquier dígito (\d) en la cadena
        patron = re.compile(r'^[a-zA-Z]{1,50}$')

        # Verificar la longitud de la cadena

        # Comprobar si la cadena cumple con ambas condiciones
        if patron.match(cadena) or cadena == '':
            return True
        else:
            return False
    
    def validate_Code(self,cadena):
        if cadena.isdigit() or cadena == '':
            return True
        else:
            return False
    
    def validate_Code_Add(self,cadena):
        if cadena.isdigit() :
            return True
        else:
            return False

    def validate_String_Add(self,cadena):
        # Expresión regular que busca cualquier dígito (\d) en la cadena
        patron = re.compile(r'^[a-zA-Z]{1,50}$')

        # Verificar la longitud de la cadena

        # Comprobar si la cadena cumple con ambas condiciones
        if patron.match(cadena) :
            return True
        else:
            return False

    def validate_NameSearch(self):
        string =  self.txt_nombreBusqueda.get()
        resultado = False
        if(not self.validate_String(string)):
            self.stateEntryName = False
            self.labelWarnigName.configure(text="Formato Incorrecto")
            resultado =  False
        else:
            self.labelWarnigName.configure(text="")     
            self.stateEntryName = True 
            resultado = True
        self.validate_btn_search()
        return resultado
    
    def validate_CodeSearch(self):
        string =  self.txt_codigoBusqueda.get()
        resultado = False
        if(not self.validate_Code(string)):
            self.stateEntryCodigo = False
            self.labelWarnigCodigo.configure(text="Formato Incorrecto")
            resultado =  False
        else:
            self.labelWarnigCodigo.configure(text="")     
            self.stateEntryCodigo = True 
            resultado = True
        self.validate_btn_search()
        return resultado
          
    def validate_btn_search(self):
        if(self.stateEntryName and self.stateEntryCodigo):
           self.btn_buscarLibro.config(state=tk.NORMAL)
        else:
           self.btn_buscarLibro.config(state=tk.DISABLED)
    
    def validate_NameAdd(self):
        string =  self.txt_nombreLibro.get()
        resultado = False
        if(not self.validate_String_Add(string)):
            self.stateEntryNombreAdd = False
            self.labelWarnigNameAdd.configure(text="Formato Incorrecto")
            resultado =  False
        else:
            self.labelWarnigNameAdd.configure(text="")     
            self.stateEntryNombreAdd = True 
            resultado = True
        self.validate_btn_add()
        return resultado
    
    def validate_CodeAdd(self):
        string =  self.txt_codigoLibro.get()
        resultado = False
        if(not self.validate_Code_Add(string)):
            self.stateEntryCodigoAdd = False
            self.labelWarnigCodeAdd.configure(text="Formato Incorrecto")
            resultado =  False
        else:
            self.labelWarnigCodeAdd.configure(text="")     
            self.stateEntryCodigoAdd = True 
            resultado = True
        self.validate_btn_add()
        return resultado
    
    def validate_AuthorAdd(self):
        string =  self.txt_autorLibro.get()
        resultado = False
        if(not self.validate_String_Add(string)):
            self.stateEntryAutorAdd = False
            self.labelWarnigAuthorAdd.configure(text="Formato Incorrecto")
            resultado =  False
        else:
            self.labelWarnigAuthorAdd.configure(text="")     
            self.stateEntryAutorAdd = True 
            resultado = True
        self.validate_btn_add()
        return resultado
    
    def validate_PricingAdd(self):
        string =  self.txt_precioLibro.get()
        resultado = False
        if(not self.validate_Code_Add(string)):
            self.stateEntryPrecioAdd = False
            self.labelWarnigPricingAdd.configure(text="Formato Incorrecto")
            resultado =  False
        else:
            self.labelWarnigPricingAdd.configure(text="")     
            self.stateEntryPrecioAdd = True 
            resultado = True
        self.validate_btn_add()
        return resultado
    
    
    def validate_btn_add(self):
        if(self.stateEntryNombreAdd and self.stateEntryCodigoAdd and self.stateEntryAutorAdd and self.stateEntryPrecioAdd and self.cb_estadoLibro.get() != "Elegi un Estado"):
           self.btn_agregarLibro.config(state=tk.NORMAL)
        else:
           self.btn_agregarLibro.config(state=tk.DISABLED)
    