from DAL.librosDAL import DALLibros
from Entities.Libro import Libro
class GestorLibro:
    dalLibros = DALLibros()
     
    def BuscarLibro(self,codigo,autor,estado):
        resultadoBusqueda = self.dalLibros.ObtenerLibros(codigo,autor,self.reasignarEstado(estado))
        listaLibros = []
        
        for item in resultadoBusqueda:
            libro = [item.idLibro,item.nombre,item.autor,self.asignarEstado(item.estado),item.precioReposicion]
            listaLibros.append(libro)
        # end for
        return listaLibros
    
    def CrearLibro(self,nombre,autor,precioReposicion):
        LibroNuevo = Libro(self.ObtenerUltimoNroLibro(),nombre,autor,precioReposicion,0)
        resultado = self.dalLibros.AgregarLibro(LibroNuevo)
        if (resultado>0): 
            return True
        return False
    
    def ActualizarLibro(self,codigo,nombre,autor,precioReposicion,estado):
        
        LibroActualizado = Libro(codigo,nombre,autor,precioReposicion,self.reasignarEstado(estado))
        resultado = self.dalLibros.ModifcarLibro(LibroActualizado)
        if (resultado>0): 
            return True
        return False
        
    def BuscarLibrosExtraviados(self,codigo,nombre,autor):
        resultado = self.dalLibros.ObtenerLibrosExtraviados(codigo,nombre,autor)
        listaLibros = []
        for item in resultado:
            libro = [item.idLibro,item.nombre,item.autor,self.asignarEstado(item.estado),item.precioReposicion]
            listaLibros.append(libro)
        return listaLibros


    def ObtenerUltimoNroLibro(self):
        ultimoNumero =  self.dalLibros.ObtenerUltimoNumeroLibro() + 1
        return ultimoNumero
    
    def asignarEstado(self,codigoEstado:int)-> str:
        if codigoEstado == 0:
            return "Disponible"
        elif codigoEstado == 1:
            return "Prestado"
        elif codigoEstado == 2:
            return "Extraviado"
        
    def reasignarEstado(self,codigoEstado:str)-> int:
        if codigoEstado == "Disponible":
            return 0
        elif codigoEstado == "Prestado":
            return 1
        elif codigoEstado == "Extraviado":
            return 2
