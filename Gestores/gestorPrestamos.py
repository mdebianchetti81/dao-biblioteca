from DAL.prestamosDAL import DALPrestamos
from Entities.Prestamo import Prestamo
from datetime import datetime, timedelta

class GestorPrestamo:
    dalPrestmo = DALPrestamos()

    def agregarPrestamo(self, idLibro, dniSocio, fechaDevolucion):
        PrestamoNuevo = Prestamo(
            self.obtenerUltimoNumero(), 
            self.obtenerIdSocio(dniSocio), 
            idLibro, 
            datetime.now().date(),
            datetime.strptime(fechaDevolucion, '%d-%m-%Y').date(),
            None,
            None,
            self.reasignarEstado('En Curso')
        )
        if(self.puedeSerPrestado(idLibro, dniSocio, fechaDevolucion)):
            resultado = self.dalPrestmo.AgregarPrestamo(PrestamoNuevo)
            return True
        else:
            return False

    def obtenerUltimoNumero(self):
        return int(self.dalPrestmo.ObtenerUltimoNumeroPrestamo()) + 1

    def obtenerIdSocio(self,dni):
        return self.dalPrestmo.ObtenerIdSocio(dni)

    def BuscarPrestamo(self, prestamo, libro, dniSocio, estado):
        resultadoBusqueda = self.dalPrestmo.ObtenerPrestamos(prestamo, libro, dniSocio, self.reasignarEstado(estado))
        listaPrestamos = []
        for item in resultadoBusqueda:
            prestamo = [item.idPrestamo,item.socio,item.libro,item.fecha_prestamo,item.fecha_devolucion,item.diasRetraso,item.fechaExtravio,self.asignarEstado(item.estado)]
            listaPrestamos.append(prestamo)
        # end for
        return listaPrestamos

    def asignarEstado(self,codigoGenero:int) -> str:
        if codigoGenero == 0:
            return "En Curso"
        elif codigoGenero == 1:
            return "Demorado"
        elif codigoGenero == 2:
            return "Finalizado"
    
    def reasignarEstado(self,codigoGenero:str) -> int:
        if codigoGenero == "En Curso":
            return 0
        elif codigoGenero == "Demorado":
            return 1
        elif codigoGenero == "Finalizado":
            return 2

    def puedeSerPrestado(self, idLibro, dniSocio, fechaDevolucion):
        # no se puede prestar si:
        ## si tiene mas de 3 libros prestados
        ## ninguno con demora 
        prestamos_socio = self.dalPrestmo.ConsultaPrestamosDelSocio(dniSocio)
        print(f'prestamos_socio', prestamos_socio)
        
        libros_prestados = len(prestamos_socio)
        libros_demorados = sum(1 for _, estado in prestamos_socio if estado == 1)
        
        print(f'prestamos_socio', prestamos_socio)
        print(f'libros_prestados', libros_prestados)
        if libros_prestados >= 3 or libros_demorados > 0:
            return False
        return True

    def ActualizarPrestamo(self, codigo, socio, libro,fechaPrestamo,fechaDevolucion,diasRetraso, fechaExtravio, estado):
        PrestamoActualizado = Prestamo(codigo, socio, libro,fechaPrestamo,fechaDevolucion,diasRetraso, fechaExtravio, self.reasignarEstado(estado))
        resultado = self.dalPrestmo.ModifcarPrestamo(PrestamoActualizado)
        return resultado

    def actualizarPrestamosDemoradosAExtraviados(self):
        fecha_actual = datetime.now()
        libros_prestados = self.dalPrestmo.ObtenerPrestamosEnDemora()
        for libro in libros_prestados:
            Id, id_libro, fecha_devolucion = libro
            fecha_devolucion = datetime.strptime(fecha_devolucion, '%Y-%m-%d')
            diferencia_dias = (fecha_actual - fecha_devolucion).days
            print(f'diferencia dias {diferencia_dias}')
            if diferencia_dias > 30:
                fecha_extravio = fecha_actual.strftime('%Y-%m-%d')
                libros_prestados = self.dalPrestmo.ActualizarPrestamosDemoradosAExtraviados(fecha_extravio, Id)

    def actualizarPrestamosEnCursoADemorados(self):
        prestamosEnCurso = self.dalPrestmo.ObtenerPrestamodEnCurso()
        for prestamo in prestamosEnCurso:
             if len(prestamo) == 4:
                    Id, id_libro, fecha_devolucion, dias_retraso = prestamo
                    if self.es_demorado(fecha_devolucion):
                        nuevos_dias_retraso = self.calcular_dias_retraso(fecha_devolucion)
                        prestamosEnCurso.append((Id, id_libro, nuevos_dias_retraso))
                        self.dalPrestmo.ActualizarPrestamosEnCursoADemorados(Id, nuevos_dias_retraso)

    def es_demorado(self, fecha_devolucion):
        fecha_actual = datetime.now()
        if(fecha_devolucion != None):
            fecha_devolucion = datetime.strptime(fecha_devolucion, '%Y-%m-%d')
            diferencia_dias = (fecha_actual - fecha_devolucion).days
            return diferencia_dias > 0
        return False

    def calcular_dias_retraso(self, fecha_devolucion):
        fecha_actual = datetime.now()
        fecha_devolucion = datetime.strptime(fecha_devolucion, '%Y-%m-%d')
        diferencia_dias = (fecha_actual - fecha_devolucion).days
        return max(0, diferencia_dias)