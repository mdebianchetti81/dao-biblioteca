from DAL.sociosDAL import DALsocios
from Entities.Socio import Socio
class GestorSocio:
    dalSocios = DALsocios()
    
    
    def BuscarSocio(self,nombre,apellido,dni):
        resultadoBusqueda = self.dalSocios.ObtenerSocios(nombre,apellido,dni)
        listaSocios = []
        
        for item in resultadoBusqueda:
            socio = [item.codigo,item.nombre,item.apellido,item.dni,self.asignarGenero(item.genero),self.asignarEstado(item.estado)]
            listaSocios.append(socio)
        # end for
        return listaSocios
    
    def CrearSocio(self,nombre,apellido,dni,genero):
        SocioNuevo = Socio(self.ObtenerUltimoNroSocio(),nombre,apellido,dni,self.reasignarGenero(genero),0)
        resultado = self.dalSocios.AgregarSocio(SocioNuevo)
        if (resultado>0): 
            return True
        return False
    
    def ActualizarSocio(self,codigo,nombre,apellido,dni,genero,estado):
        
        SocioActualizado = Socio(codigo,nombre,apellido,dni,self.reasignarGenero(genero),self.reasignarEstado(estado))
        resultado = self.dalSocios.ModifcarSocio(SocioActualizado)
        if (resultado>0): 
            return True
        return False
        
    
    def ObtenerUltimoNroSocio(self):
        ultimoNumero =  self.dalSocios.ObtenerUltimoNumeroSocio() + 1
        return ultimoNumero
    
    def asignarGenero(self,codigoGenero:int) -> str:
        if codigoGenero == 0:
            return "Femenino"
        elif codigoGenero == 1:
            return "Masculino"
        elif codigoGenero == 2:
            return "Otro"
    def asignarEstado(self,codigoEstado:int)-> str:
        if codigoEstado == 1:
            return "Activo"
        elif codigoEstado == 0:
            return "Inactivo"
        elif codigoEstado == 2:
            return "Otro"
       
    def reasignarGenero(self,codigoGenero:str)-> int:
        if codigoGenero == "Femenino":
            return 0
        elif codigoGenero == "Masculino":
            return 1
        elif codigoGenero == "Otro":
            return 2
    def reasignarEstado(self,codigoEstado:str)-> int:
        if codigoEstado == "Activo":
            return 1
        elif codigoEstado == "Inactivo":
            return 0
        elif codigoEstado == "Otro":
            return 2
