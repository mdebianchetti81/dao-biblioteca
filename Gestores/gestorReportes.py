import os
from reportlab.lib.pagesizes import letter
from reportlab.lib import colors
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer,Image
from reportlab.platypus.tables import Table, TableStyle
import matplotlib.pyplot as plt
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch
from DAL.librosDAL import DALLibros
from DAL.prestamosDAL import DALPrestamos
from DAL.sociosDAL import DALsocios

from io import BytesIO

class GestorReportes:
    
    dalLibros = DALLibros()
    dalPrestamo = DALPrestamos()
    dalSocio = DALsocios()
    
    def GenerarReporte(self):
        doc = SimpleDocTemplate("reportes.pdf",pagesize=letter)
        elements = []
        
        styles = getSampleStyleSheet()
        
        title = "Reporte de la Biblioteca"
        elements.append(Paragraph(title,styles['Title']))
        elements.append(Spacer(1, 12))
        ## Si o si Vamos a Incluir lo Cantidad de Libros Segun Estados
        # y lo de Sumatoria de la reposicion de los libros extraviados
        
        elements.append(Paragraph("Cantidad de libros Segun Estado",styles['Heading2']))
        elements.append(Spacer(1, 12))
        listaxEstado = self.dalLibros.ObtenerCantidadLibros()
      
        
        labels = ["Disponible","Prestado","Extraviado"]
        datosTabla = [labels] + [listaxEstado]
        tabla = Table(datosTabla)
        tabla.setStyle(TableStyle([('BACKGROUND', (0, 0), (-1, 0), colors.grey),
                                ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),
                                ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                                ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
                                ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
                                ('BACKGROUND', (0, 1), (-1, -1), colors.beige)]))
        #labels = ["Disponible"]
        elements.append(tabla)

        elements.append(Spacer(1, 12))
        plt.pie(listaxEstado,labels=labels,autopct='%1.1f%%')
        plt.axis('equal')
        
        buffer = BytesIO()
        plt.savefig(buffer, format='png')
        buffer.seek(0)
        elements.append(Image(buffer, width=525, height=450))
        elements.append(Spacer(1, 12))
        doc.build(elements)
        
    def GenerarListadoPrestamoDeSocio(self, reporteNombre, idSocio):
        try:
            # Obtener la ruta completa para la carpeta "Recursos/Reportes"
            carpeta_reportes = os.path.join(os.getcwd(), "Recursos", "Reportes")

            # Verificar si la carpeta existe, si no, crearla
            if not os.path.exists(carpeta_reportes):
                os.makedirs(carpeta_reportes)

            ruta_informe = os.path.join(carpeta_reportes, reporteNombre)
            doc = SimpleDocTemplate(ruta_informe, pagesize=letter)
            elements = []

            encabezados = ['ID Prestamo', 'ID Libro', 'Fecha Prestamo', 'Fecha Devolucion', 'Dias de Retraso', 'Fecha Extravio', 'Estado']
            listaXSocio = self.dalPrestamo.ObtenerPrestamoXsocio(idSocio)
            datos_tabla = [encabezados] + listaXSocio

            tabla = Table(datos_tabla)
            tabla.setStyle(TableStyle([('BACKGROUND', (0, 0), (-1, 0), colors.grey),
                                ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),
                                ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                                ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
                                ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
                                ('BACKGROUND', (0, 1), (-1, -1), colors.beige)]))
            # Agregar la tabla al PDF
            elements.append(tabla)

            elements.append(Spacer(1, 12))

            cantidad_prestamos = len(listaXSocio)
            cantidad_en_curso = sum(1 for prestamo in listaXSocio[1:] if prestamo[-1] == 0)
            cantidad_demorados = sum(1 for prestamo in listaXSocio[1:] if prestamo[-1] == 1)

            # Agregar texto adicional al PDF
            styles = getSampleStyleSheet()
            texto_adicional = f"Cantidad de préstamos: {cantidad_prestamos}\n" \
                                f"Cantidad de préstamos en curso: {cantidad_en_curso}\n" \
                                f"Cantidad de préstamos demorados: {cantidad_demorados}\n"
            elements.append(Paragraph(texto_adicional, styles['Normal']))

            doc.build(elements)
            return True
        except Exception as e:
            raise e
        else:
            return False

    def ObtenerListadoPrestamosDemorados(self, reporteNombre):

        try:
            # Obtener la ruta completa para la carpeta "Recursos/Reportes"
            carpeta_reportes = os.path.join(os.getcwd(), "Recursos", "Reportes")

            # Verificar si la carpeta existe, si no, crearla
            if not os.path.exists(carpeta_reportes):
                os.makedirs(carpeta_reportes)
            ruta_informe = os.path.join(carpeta_reportes, reporteNombre)

            doc = SimpleDocTemplate(ruta_informe, pagesize=letter)
            elements = []

            encabezados = ['ID Prestamo', 'ID Libro', 'Fecha Prestamo', 'Fecha Devolucion', 'Dias de Retraso', 'Fecha Extravio', 'Estado']
            listaPrestamosDemorados = self.dalPrestamo.ObtenerPrestamosDemorados()
            datos_tabla = [encabezados] + listaPrestamosDemorados

            tabla = Table(datos_tabla)
            tabla.setStyle(TableStyle([('BACKGROUND', (0, 0), (-1, 0), colors.grey),
                                ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),
                                ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                                ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
                                ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
                                ('BACKGROUND', (0, 1), (-1, -1), colors.beige)]))
            # Agregar la tabla al PDF
            elements.append(tabla)

            elements.append(Spacer(1, 12))

            cantidad_prestamos = len(listaPrestamosDemorados)

            # Agregar texto adicional al PDF
            styles = getSampleStyleSheet()
            texto_adicional = f"Cantidad de préstamos: {cantidad_prestamos}\n"

            elements.append(Paragraph(texto_adicional, styles['Normal']))

            doc.build(elements)
            return True
        except Exception as e:
            raise e
        else:
            return False

    def GenerarSumatoriaPrecioReposicion(self):
        estado = 2 #extraviado
        return self.dalLibros.ObtenerPrecioReposicionLibros(estado)

    def ObtenerLibroDeSocios(self, reporteNombre, nombreLibro):

        try:
            # Obtener la ruta completa para la carpeta "Recursos/Reportes"
            carpeta_reportes = os.path.join(os.getcwd(), "Recursos", "Reportes")

            # Verificar si la carpeta existe, si no, crearla
            if not os.path.exists(carpeta_reportes):
                os.makedirs(carpeta_reportes)
            ruta_informe = os.path.join(carpeta_reportes, reporteNombre)

            doc = SimpleDocTemplate(ruta_informe, pagesize=letter)
            elements = []

            encabezados = ['ID Libro', 'Nombre', 'Estado', 'Autor', 'Precio Reposicion']
            listaSociosXLibro = self.dalSocio.ObtenerNombreSociosAPartirDeNombre(nombreLibro)
            datos_tabla = [encabezados] + listaSociosXLibro

            tabla = Table(datos_tabla)
            tabla.setStyle(TableStyle([('BACKGROUND', (0, 0), (-1, 0), colors.grey),
                                ('TEXTCOLOR', (0, 0), (-1, 0), colors.whitesmoke),
                                ('ALIGN', (0, 0), (-1, -1), 'CENTER'),
                                ('FONTNAME', (0, 0), (-1, 0), 'Helvetica-Bold'),
                                ('BOTTOMPADDING', (0, 0), (-1, 0), 12),
                                ('BACKGROUND', (0, 1), (-1, -1), colors.beige)]))
            # Agregar la tabla al PDF
            elements.append(tabla)

            elements.append(Spacer(1, 12))

            cantidad_solicitantes = len(listaSociosXLibro)

            # Agregar texto adicional al PDF
            styles = getSampleStyleSheet()
            texto_adicional = f"Cantidad de solicitantes del libro: {cantidad_solicitantes}\n"

            elements.append(Paragraph(texto_adicional, styles['Normal']))

            doc.build(elements)
            return True
        except Exception as e:
            return False
        else:
            return False
