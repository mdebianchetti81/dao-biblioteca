from datetime import datetime, timedelta

class Prestamo():
    def __init__(self, idPrestamo, socio, libro, fecha_prestamo, fecha_devolucion, diasRetraso, fechaExtravio, estado):
        # self.cant_dias = cant_dias
        self.idPrestamo = idPrestamo
        self.socio = socio
        self.libro = libro
        self.fecha_prestamo = fecha_prestamo
        self.fecha_devolucion = fecha_devolucion
        self.diasRetraso = diasRetraso
        self.fechaExtravio = fechaExtravio
        self.estado = estado #estados posibles: En curso, Demorado, Finalizado

    def calcularFechaDevolucion(self, cant_dias):
        fecha_actual = datetime.now().date()
        fecha_devolucion = fecha_actual + timedelta(days=int(cant_dias))
        print(f'fecha dev ${fecha_devolucion}')
        return fecha_devolucion
