from pathlib import Path
import sqlite3

class SQL:
    dbconnection = None
    _instance = None
    cursor =None
    
    def __new__(cls,connection_string :str):
        if not cls._instance:
            cls._instance = super(SQL,cls).__new__(cls)
            cls.dbconnection = sqlite3.Connection(connection_string)
            cls.cursor = cls.dbconnection.cursor()
        return cls._instance
    
    def creardb(self,nameBase):
        if(not (Path('./Biblioteca.db')).exists()):
            cnx = sqlite3.connect('Biblioteca.db')
            cursor = cnx.cursor()
            cursor.execute('''
                    CREATE TABLE IF NOT EXISTS Socios
                    (
                        Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
                        idSocio INTEGER NOT NULL, 
                        nombre TEXT NULL, 
                        apellido TEXT NULL, 
                        dni TEXT NOT NULL,
                        genero INTEGER NOT NULL,
                        estado INTGER NOT NULL
                    )''')
            cursor.execute('''
                    CREATE TABLE IF NOT EXISTS Libros
                    (
                        Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
                        idLibro INT NOT NULL, 
                        nombre TEXT NOT NULL, 
                        estado INT NOT NULL, 
                        autor TEXT  NULL,
                        precioReposicion REAL NOT NULL
                    )''')
            cursor.execute('''
                    CREATE TABLE IF NOT EXISTS Prestamos
                    (
                        Id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
                        idSocio INTEGER NOT NULL, 
                        idLibro INTEGER NOT NULL, 
                        fechaPrestamo DATE NOT NULL, 
                        fechaDevolucion DATE NULL, 
                        diasDeRetraso int NULL, 
                        fechaExtravio DATE,
                        estado int NOT NULL
                    )''')
            #Ejemplo de Insert de Socio
            cursor.execute("INSERT INTO Socios ( idSocio, nombre, apellido, dni,genero,estado) VALUES ( 1, 'Juan',  'Villarruel', '41711515',1,1)")
            cursor.execute("INSERT INTO Socios ( idSocio, nombre, apellido, dni,genero,estado) VALUES ( 2, 'Nicolas', 'Villalba', '40251535',1,1)")
            #Ejemplo de Insert de Libro
            cursor.execute("INSERT INTO Libros ( idLibro, nombre, estado, autor,precioReposicion) VALUES (1, 'Hobbit', 0, 'J. R. R. Tolkien',1500.0)")
            #Ejemplo de Inser Prestamos
            cursor.execute("INSERT INTO Prestamos ( idSocio, idLibro, fechaPrestamo, fechaDevolucion,diasDeRetraso,fechaExtravio,estado) VALUES (1, 1, '11-11-2023','11-12-2023',15,2)")
                    
            cnx.commit()
            cnx.close()
            
    def execute_sql_sp(self,sql_line):
        resultado = ""
        try:
            cursor = self.cursor
            cursor.execute(sql_line)
            resultado = cursor.fetchall()
            self.dbconnection.commit()
        except sqlite3.Error as e:
            # Handle SQLite errors
            print(f"SQLite error: {e}")

        except Exception as e:
            # Handle other exceptions
            print(f"An unexpected error occurred: {e}")
        return resultado

    def excute_sql_DeleteUpdateInsert(self,sql_line):
        try:
            cursor = self.cursor
            cursor.execute(sql_line)
            cursor.execute("SELECT changes()")
            filasAfectadas = cursor.fetchall()[0][0]
            if(filasAfectadas > 0):
                self.dbconnection.commit()
        except sqlite3.Error as e:
            # Handle SQLite errors
            print(f"SQLite error: {e}")

        except Exception as e:
            # Handle other exceptions
            print(f"An unexpected error occurred: {e}")
        return filasAfectadas