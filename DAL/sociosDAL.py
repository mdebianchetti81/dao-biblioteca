from DAL.dataAccess import SQL
from Entities.Socio import Socio
class DALsocios:
    sql = None
    def __init__(self) -> None:
        self.sql = SQL("Biblioteca.db")
    def ObtenerSocios(self,nombre,apellido,dni):
        listaSocios:list[Socio] = []
        stringrequets = "SELECT * FROM Socios s WHERE 0=0 "
        if(nombre != '')    : 
           stringrequets = stringrequets + f" AND s.nombre    LIKE '%{nombre}%'"
        if(apellido != '')  : 
           stringrequets = stringrequets + f" AND s.apellido  LIKE '%{apellido}%'"
        if(dni != '')       : 
           stringrequets = stringrequets + f" AND s.dni       LIKE '%{dni}%'"
        
        for socio in self.sql.execute_sql_sp(stringrequets):
            socioObtenido = Socio(socio[1],socio[2],socio[3],socio[4],socio[5],socio[6])
            listaSocios.append(socioObtenido)
        return listaSocios
    
    def ModifcarSocio(self,socio):
        stringRequest = f"UPDATE Socios SET idSocio={socio.codigo},nombre='{socio.nombre}',apellido='{socio.apellido}',dni='{socio.dni}',genero={socio.genero},estado={socio.estado} WHERE idSocio={socio.codigo}"        
        resultado = self.sql.excute_sql_DeleteUpdateInsert(stringRequest)
        if (resultado>0): 
            return True
        return False        
    
    def EliminarSocio(self,socio):
        pass
    
    def AgregarSocio(self,socio:Socio):
        stringRequest = f"INSERT INTO Socios ( idSocio, nombre, apellido, dni,genero,estado) VALUES({socio.codigo},'{socio.nombre}','{socio.apellido}','{socio.dni}',{socio.genero},{socio.estado})" 
        resultado = self.sql.excute_sql_DeleteUpdateInsert(stringRequest)
        if (resultado>0): 
            return True
        return False

    def ObtenerUltimoNumeroSocio(self):
        stringRequest = "SELECT s.idSocio FROM Socios s ORDER BY idSocio DESC LIMIT 1 "
        resultado = self.sql.execute_sql_sp(stringRequest)
        print(resultado[0][0])    
        return int(resultado[0][0])

    def ObtenerNombreSociosAPartirDeNombre(self, titulo):
        stringRequest = f"SELECT Socios.nombre FROM Socios JOIN Prestamos ON Socios.idSocio = Prestamos.idSocio JOIN Libros ON Prestamos.idLibro = Libros.idLibro WHERE Libros.nombre like '%{titulo}%';"
        return self.sql.execute_sql_sp(stringRequest)