from DAL.dataAccess import SQL
from Entities.Libro import Libro

class DALLibros:       
    sql = None
    def __init__(self) -> None:
        self.sql = SQL("Biblioteca.db")

    def ObtenerLibros(self,codigo,autor,estado):
        listaLibros:list[Libro] = []
        
        stringrequets = "SELECT * FROM Libros l WHERE 0=0 "
        if(codigo != '')    : 
           stringrequets = stringrequets + f" AND l.idLibro    LIKE '%{codigo}%'"
        if(autor != '')  : 
           stringrequets = stringrequets + f" AND l.autor  LIKE '%{autor}%'"
        if(estado != None): 
           stringrequets = stringrequets + f" AND l.estado  = '{estado}'"
        
        for libro in self.sql.execute_sql_sp(stringrequets):
            libroObtenido = Libro(libro[1],libro[2],libro[4],libro[5],libro[3])
            listaLibros.append(libroObtenido)
        # end for
        return listaLibros
    
    def ModifcarLibro(self,libro:Libro):
        stringRequest = f"UPDATE Libros SET idLibro={libro.idLibro},nombre='{libro.nombre}',autor='{libro.autor}',precioReposicion='{libro.precioReposicion}',estado={libro.estado} WHERE idLibro={libro.idLibro}"        
        resultado = self.sql.excute_sql_DeleteUpdateInsert(stringRequest)
        if (resultado>0): 
            return True
        return False
            
    def EliminarLibro(self,libro):
        pass
    
    def AgregarLibro(self,libro:Libro):
        stringRequest = f"INSERT INTO Libros ( idLibro, nombre, autor, precioReposicion,estado) VALUES({libro.codigo},'{libro.nombre}','{libro.autor}','{libro.precioReposicion}',{libro.estado})" 
        resultado = self.sql.excute_sql_DeleteUpdateInsert(stringRequest)
        if (resultado>0): 
            return True
        return False        
        
    def ObtenerUltimoNumeroLibro(self):
        stringRequest = "SELECT s.idLibro FROM Libros l ORDER BY l.idLibro DESC LIMIT 1 "
        resultado = self.sql.execute_sql_sp(stringRequest)
        print(resultado[0][0])    
        return int(resultado[0][0])
    
    
    def ObtenerLibrosExtraviados(self,codigo,nombre,autor):
        stringRequest = f"SELECT L.* FROM Libros L WHERE L.estado = 2 "
        if(codigo != ""):          
            stringRequest = stringRequest + f"AND L.idLibro like '%{codigo}%' "
        if(nombre != ""):          
            stringRequest = stringRequest + f"AND L.nombre like '%{nombre}%' "
        if(autor != ""):          
            stringRequest = stringRequest + f"AND L.autor like '%{autor}%' "       
        
        listaLibros = []
        for libro in self.sql.execute_sql_sp(stringRequest):
            libroObtenido = Libro(libro[1],libro[2],libro[4],libro[5],libro[3])
            listaLibros.append(libroObtenido)
        # end for
        return listaLibros
        
    def ObtenerCantidadLibros(self):
        listaCantidades = []
        stringRequest = "SELECT estado, COUNT(*) as cantidad FROM Libros GROUP BY estado HAVING COUNT(*) > 0"
        for cantidadxestado in self.sql.execute_sql_sp(stringRequest):
            listaCantidades.insert(int(cantidadxestado[0]),cantidadxestado[1])        
        return listaCantidades

    def ObtenerPrecioReposicionLibros(self, estado):
        stringRequest = f"SELECT SUM(precioReposicion) FROM Libros WHERE estado = {estado};"
        result = self.sql.execute_sql_sp(stringRequest)
        return float(result[0][0]) if result[0][0] else 0