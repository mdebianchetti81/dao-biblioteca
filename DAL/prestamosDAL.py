from DAL.dataAccess import SQL
from Entities.Prestamo import Prestamo
from datetime import datetime, timedelta

class DALPrestamos:

   def __init__(self) -> None:
      self.sql = SQL("Biblioteca.db")
      self.sql.creardb("nombre_de_la_base_de_datos")

   def ObtenerPrestamos(self,prestamo, libro, dniSocio, estado):
      listaPrestamos:list[Prestamo] = []

      stringrequets = "SELECT * FROM Prestamos p WHERE 0=0 "
      if(libro != '')    :
         stringrequets = stringrequets + f" AND p.idLibro    LIKE '%{libro}%'"
      if(prestamo != '')  :
         stringrequets = stringrequets + f" AND p.Id  LIKE '%{prestamo}%'"
      if(estado != None)  :
         stringrequets = stringrequets + f" AND p.estado  LIKE '%{estado}%'"
      if(dniSocio != '')  :
         stringrequets = stringrequets + f" AND p.idSocio  LIKE '%{dniSocio}%'"
      for prestamo in self.sql.execute_sql_sp(stringrequets):
            prestamoObtenido = Prestamo(prestamo[0], prestamo[1],prestamo[2],prestamo[3], prestamo[4],prestamo[5],prestamo[6],prestamo[7])
            listaPrestamos.append(prestamoObtenido)
      # end for
      return listaPrestamos

   def ModifcarPrestamo(self,prestamo: Prestamo):
      try:
         stringrequets = f"UPDATE Prestamos SET idSocio='{prestamo.socio}', idLibro='{prestamo.libro}', fechaPrestamo='{prestamo.fecha_prestamo}' "
         if(prestamo.fecha_devolucion != '' and prestamo.fecha_devolucion != None and prestamo.fecha_devolucion != 'None')  :
            stringrequets = stringrequets + f", fechaDevolucion = '{prestamo.fecha_devolucion}'"
         if(prestamo.diasRetraso != '' and prestamo.diasRetraso != None and prestamo.diasRetraso != 'None'):
            stringrequets = stringrequets + f", diasDeRestrado = {prestamo.diasRetraso}"
         if(prestamo.fechaExtravio != '' and prestamo.fechaExtravio != None and prestamo.fechaExtravio != 'None'):
            stringrequets = stringrequets + f", fechaExtravio = '%{prestamo.fechaExtravio}%'"
         if(prestamo.estado != None):
            stringrequets = stringrequets + f", estado = {prestamo.estado}"
         stringrequets = stringrequets + f" WHERE Id={prestamo.idPrestamo}"
         resultado = self.sql.execute_sql_sp(stringrequets)
         return True
      except Exception as e:
         return False
         raise e

   def AgregarPrestamo(self, prestamo:Prestamo):
      stringRequest = f"UPDATE Libros SET estado = 1 WHERE idLibro = {prestamo.libro}"
      self.sql.execute_sql_sp(stringRequest)
      stringRequest = f"INSERT INTO Prestamos (idSocio, idLibro, fechaPrestamo, fechaDevolucion, estado) VALUES({prestamo.socio}, {prestamo.libro} , '{prestamo.fecha_prestamo}','{prestamo.fecha_devolucion}', '{prestamo.estado}')"
      self.sql.execute_sql_sp(stringRequest)

   def ConsultaPrestamosDelSocio(self, idSocio):
      stringRequest = f"SELECT idLibro, estado FROM Prestamos WHERE idSocio = {idSocio} AND estado IN (0, 1)"
      return self.sql.execute_sql_sp(stringRequest)

   def ObtenerPrestamoXsocio(self, idSocio):
      stringRequest = f"SELECT * FROM Prestamos WHERE idSocio = {idSocio};"
      return self.sql.execute_sql_sp(stringRequest)

   def ObtenerPrestamosDemorados(self):
      stringRequest = f"SELECT * FROM Prestamos WHERE estado = 1"
      return self.sql.execute_sql_sp(stringRequest)

   def ObtenerPrestamosEnDemora(self):
      stringRequest = f"SELECT Id, idLibro, fechaDevolucion FROM Prestamos WHERE estado = 1"
      return self.sql.execute_sql_sp(stringRequest)

   def ActualizarPrestamosDemoradosAExtraviados(self, fecha_extravio, idPrestamo):
      stringRequest = f"UPDATE Prestamos SET estado = 2, fechaExtravio = {fecha_extravio} WHERE Id = {idPrestamo}"
      return self.sql.execute_sql_sp(stringRequest)

   def ObtenerPrestamodEnCurso(self):
      stringRequest = f"SELECT Id, idLibro, fechaDevolucion, diasDeRestrado FROM Prestamos WHERE estado = 0"
      return self.sql.execute_sql_sp(stringRequest)

   def ActualizarPrestamosEnCursoADemorados(self, idPrestamo, nuevos_dias_retraso):
      stringRequest = f"UPDATE Prestamos SET estado = 1, diasDeRestrado = {nuevos_dias_retraso} WHERE Id = {idPrestamo}"
      return self.sql.execute_sql_sp(stringRequest)
   
   def ObtenerUltimoNumeroPrestamo(self):
        stringRequest = "SELECT id FROM Prestamos  ORDER BY id DESC LIMIT 1 "
        resultado = self.sql.execute_sql_sp(stringRequest)
        print(resultado[0][0])    
        return int(resultado[0][0])
     
   def ObtenerIdSocio(self,dni):
      stringRequest = f"SELECT idSocio FROM Socios where dni={dni}"
      resultado = self.sql.execute_sql_sp(stringRequest)
      return resultado[0][0]